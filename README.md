# Overview
Groolenium is a productivity improving Web testing framework that is based on Selenium WebDriver. The key purpose of the project is to establish an opinionated set of base tools and APIs for starting UI testing with less infrastructural fuzz and more focus and fun.

# Key features
* Groovy support
* Configuration support
* Enhanced Page Objects
* Separated Javascript
* Dependency Injection
* JQuery selectors support
* Associated REST client
* Maven starter POMs
* JUnit support
* 


# TBD
TBD