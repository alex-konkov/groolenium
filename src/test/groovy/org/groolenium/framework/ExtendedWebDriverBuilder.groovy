package org.groolenium.framework

import org.groolenium.framework.webdriver.ConfiguredExtendedWebDriverBuilder
import org.groolenium.framework.webdriver.ExtendedWebDriver
import org.groolenium.framework.webdriver.factory.LocalChromeWebDriverFactory
import org.groolenium.framework.webdriver.factory.LocalHtmlUnitWebDriverFactory
import org.groolenium.framework.webdriver.factory.LocalSafariWebDriverFactory
import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.DesiredCapabilities

/**
 * Stands for building pre-configured WebDriver instances for testing the framework.
 *
 * @author Alex K
 */
class ExtendedWebDriverBuilder {

    public static final String BLANK_HTML_URL = ExtendedWebDriverBuilder.getClassLoader().getResource('blank.html').toString()

    private static final Map<String, Closure<WebDriver>> LOCAL_BROWSER_FACTORIES = [
            "chrome" : {
                def capabilities = new DesiredCapabilities([:])
                new LocalChromeWebDriverFactory(capabilities).createWebDriver()
            },
            "safari" : {
                def capabilities = new DesiredCapabilities([:])
                new LocalSafariWebDriverFactory(capabilities).createWebDriver()
            },
            "htmlunit" : {
                Capabilities capabilities = new DesiredCapabilities(
                        browserName: 'firefox', version: '3', javascriptEnabled: true
                )
                new LocalHtmlUnitWebDriverFactory(capabilities).createWebDriver()
            }
    ]

    /**
     * Creates a local WebDriver which is already navigated to the local blank page.
     *
     * @return the newly built extended WebDriver instance
     */
    static ExtendedWebDriver getLocal() {
        ExtendedWebDriver result
        def specifiedBrowser = System.getProperty('tests.local.browser')
        if (specifiedBrowser && LOCAL_BROWSER_FACTORIES.containsKey(specifiedBrowser)) {
            result = newExtended(LOCAL_BROWSER_FACTORIES.get(specifiedBrowser).call())
        } else {
            result = newExtended(LOCAL_BROWSER_FACTORIES.get('htmlunit').call())
        }
        result.navigate().to BLANK_HTML_URL
        result
    }

    private static ExtendedWebDriver newExtended(WebDriver driver) {
        new ConfiguredExtendedWebDriverBuilder(driver).build()
    }


}
