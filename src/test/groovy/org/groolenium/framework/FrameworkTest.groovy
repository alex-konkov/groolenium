package org.groolenium.framework

import org.groolenium.framework.page.IgnoreBindingAwareElementLocatorFactory
import org.groolenium.framework.page.PageObject
import org.groolenium.framework.page.PageObjectBuilder
import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.NotFoundException
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebElement
import org.testng.annotations.Test

import static org.testng.Assert.*

/**
 * Various framework tests intended for testing test components functionality.
 *
 * @author Alex K
 */
class FrameworkTest extends WebDriverBackedTest {

    public static final String BAD_JAVASCRIPT = UUID.randomUUID().toString()

    @Test
    void testExecuteScriptReturn() {
        webDriver.with {
            assertEquals executeScriptReturn('5'), 5
            assertEquals executeScriptReturn('5').getClass(), Long.class
            assertEquals executeScriptReturn('"6"'), '6'
            assertEquals executeScriptReturn('"6"').getClass(), String.class
            assertEquals executeScriptReturn('true'), true
            assertEquals executeScriptReturn('true').getClass(), Boolean.class
            assertEquals executeScriptReturn('6'), executeScript('return 6;')
            assertEquals executeScriptReturn("window['${UUID.randomUUID()}']"), null // not an 'undefined'!
            assertEquals executeScriptReturn(''), null
            assertTrue(executeScriptReturn('{}') instanceof Map)
            assertTrue(executeScriptReturn('{a:2}') == [a:2])
            assertTrue(executeScriptReturn('[]') instanceof List)
            assertTrue(executeScriptReturn('[3,7]') == [3,7])
        }
    }

    @Test
    void testConfig() {
        System.setProperty('one.two.threeStr', '4')
        System.setProperty('one.two.fourInt', '5')
        def conf = new Configuration('''
            one {
                two {
                   threeStr = '3'
                   fourInt = 4
                }
            }''').configObject
        assertEquals('4', conf.one.two.threeStr) // value is changed to '4'
        assertEquals(String, conf.one.two.threeStr.getClass()) // type is String
        assertEquals(5, conf.one.two.fourInt) // value is changed to 5
        assertEquals(Integer, conf.one.two.fourInt.getClass()) // still Integer
    }

    @Test
    void testWaitForNonExistenceOfNonExistentItem() {
        // waiting until our random var is empty
        webDriver.shortWait.until {
            !isRandomExist()
        }
    }

    @Test
    void testWaitForExistenceOfNonExistentItem() {
        def waitTimeoutSec = 1
        def tookMs = Utils.getExecutionTimeMs {
            try {
                // waiting until our random var is not empty, it must never happen
                webDriver.getWait(waitTimeoutSec, 400).until {
                    isRandomExist()
                }
                fail("mustn't reach here, exception expected but not thrown")
            } catch (TimeoutException e) {
                // expected
            }
        }
        assertTrue tookMs > waitTimeoutSec * 1000
    }

    @Test(expectedExceptions = Exception.class)
    void testInvalidJavascript() {
        webDriver.executeScript(BAD_JAVASCRIPT)
    }

    @Test
    void debugInfo() {
        def writeProps = { p ->
            p.each { println "${it.key}=${it.value}" }
        }
        println "= = = SYSTEM PROPERTIES = = ="
        writeProps System.getProperties()
        println "= = = TESTS CONFIG (FLATTENED) = = ="
        writeProps Configuration.get().toProperties()
        println "= = = GROOVY LIB = = ="
        println GString.protectionDomain.codeSource.location.path
    }

    class SamplePage extends PageObject {
        boolean isAvailable() { true }
    }

    class SampleNeverLoadPage extends PageObject {
        boolean isAvailable() { false }
    }

    @Test(expectedExceptions = TimeoutException.class)
    void testPageObjectInitTimeout() {
        // using builder with specific settings
        def builder = new PageObjectBuilder(webDriver, webDriver.getWait(1,400), IgnoreBindingAwareElementLocatorFactory)
        builder.createPageObject(SampleNeverLoadPage.class)
    }

    @Test(expectedExceptions = NoSuchElementException)
    void testFindElementThrowsException() {
        webDriver.findElement(By.id('nonExistentId'))
    }

    @Test(expectedExceptions = TimeoutException)
    void testSkippingOfNotFoundException() {
        // the following wait has to generate timeout and ignore
        // happening exception about not found element
        webDriver.getWait(1,300).until {
            throw new NotFoundException('msg')
        }
    }

    private def isRandomExist() {
        webDriver.executeScriptReturn("window['${UUID.randomUUID()}']") != null
    }

    private PageObjectBuilder createPageObjectBuilderForTests() {
        new PageObjectBuilder(webDriver, webDriver.longWait, IgnoreBindingAwareElementLocatorFactory)
    }

}

interface JavaScriptObject {

    String execute()

}

interface JavaScriptObjectWithSourceInTheRoot {

    String execute()

}

interface JavaScriptObjectWithoutSource { }

interface WebElementManipulation {

    WebElement getDocument()

    WebElement addDivTo(WebElement el)

}
