package org.groolenium.framework.page

import org.groolenium.framework.WebDriverBackedTest
import org.groolenium.framework.page.pos.AnnotatedPageObject
import org.groolenium.framework.page.pos.AnnotatedPageObjectWithInnerJsInline
import org.groolenium.framework.page.pos.DelegatingExtendedPageObject
import org.groolenium.framework.page.pos.DelegatingExtendedWithInjection
import org.groolenium.framework.page.pos.ExtendedDynamicPageObject
import org.groolenium.framework.page.pos.ExtendedPageObjectWithOverridingJSMethod
import org.groolenium.framework.page.pos.ExtendedWithOverridingProp
import org.groolenium.framework.page.pos.PageObjectWithoutJSSources
import org.groolenium.framework.page.pos.SimpleDynamicPageObject
import org.groolenium.framework.webdriver.ExtendedWebDriverWait
import org.groolenium.framework.webdriver.ExtendedWebElement
import org.openqa.selenium.*
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.Assert
import org.testng.annotations.Test

/**
 * Various tests for page object building.
 *
 * @author Alex K
 */
class PageObjectBuilderTest extends WebDriverBackedTest {

    private static final int PAGEOBJECT_WAIT_TIMEOUT_SEC = 1
    private static final int PAGEOBJECT_WAIT_TIMEOUT_MS = PAGEOBJECT_WAIT_TIMEOUT_SEC * 1000
    private static final int PAGEOBJECT_POLLING_PERIOD_MS = 300
    private static final int HALF_A_PAGEOBJECT_WAIT_TIMEOUT_MS = (int) (PAGEOBJECT_WAIT_TIMEOUT_MS / 2)

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class SimplePageObject extends PageObject {
        @FindBy(css = ".some-class")
        WebElement someDiv
    }

    @Test
    void testSimplePageObjectCreation() {
        setBodyHtml("""
            <div id="some-id" class="some-class">
            </div>
        """)
        def pageObject = createPageObject SimplePageObject
        Assert.assertEquals(pageObject.someDiv.getAttribute('id'), 'some-id')
    }

    @Test
    void testPageObjectCreationWithNonExistentFields() {
        setBodyHtml("")
        def pageObject = createPageObject SimplePageObject
        assertThrows(NoSuchElementException, {
            // this has to be the first time the field gets searched
            pageObject.someDiv.isDisplayed()
        })
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class NeverAvailablePageObject extends PageObject {
        @Override
        boolean isAvailable() {
            false
        }
    }

    @Test
    void testWaitingForPageObjectAvailability() {
        setBodyHtml("")
        long msBefore = System.currentTimeMillis()
        assertThrows(TimeoutException, {
            createPageObject NeverAvailablePageObject
        })
        long took = System.currentTimeMillis() - msBefore
        Assert.assertTrue(took >= PAGEOBJECT_WAIT_TIMEOUT_MS, "took $took;")
        Assert.assertTrue(took < PAGEOBJECT_WAIT_TIMEOUT_MS + /* ~ */ 500)
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class AppearingLaterPageObject extends PageObject {

        @FindBy(id = 'some-id')
        WebElement someDiv

        @Override
        boolean isAvailable() {
            return !webDriver.findElements(By.id('some-id')).isEmpty()
        }
    }

    @Test
    void testTimeOfWaitingForPageObjectAvailability() {
        setBodyHtml("")
        long msBefore = System.currentTimeMillis()
        setBodyHtml("""
            <div id="some-id">text</div>
        """, HALF_A_PAGEOBJECT_WAIT_TIMEOUT_MS)
        def pageObject = createPageObject AppearingLaterPageObject
        long took = System.currentTimeMillis() - msBefore
        Assert.assertTrue(took >= HALF_A_PAGEOBJECT_WAIT_TIMEOUT_MS, "took $took;")
        Assert.assertTrue(pageObject.someDiv.isDisplayed())
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class PageObjectForDefaultWiring extends PageObject {
        WebElement someId
        WebElement someName
    }

    @Test
    void testDefaultWebElementWiring() {
        setBodyHtml("""
            <div id="someId" class="some-class">first</div>
            <form>
                Input: <input type="text" name="someName" class="input-class">
            </form>
        """)
        def pageObject = createPageObject PageObjectForDefaultWiring
        Assert.assertNotNull(pageObject.someId)
        Assert.assertEquals(pageObject.someId.getAttribute('class'), 'some-class')
        Assert.assertNotNull(pageObject.someName)
        Assert.assertEquals(pageObject.someName.getAttribute('class'), 'input-class')
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class PageObjectWithSuppressedDefaultWiring extends PageObject {
        @IgnoreBinding
        WebElement someDiv
    }

    @Test
    void testSuppressingOfDefaultWebElementWiring() {
        setBodyHtml("""
            <div id="someDiv"/>
        """)
        def pageObject = createPageObject PageObjectWithSuppressedDefaultWiring
        Assert.assertNull(pageObject.someDiv)
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class RootContextAwarePageObject extends PageObject {
        @FindBy(id="some-div-id")
        WebElement someDiv

        @Override
        WebElement getRootElement() throws NotFoundException {
            webDriver.findElement(By.id("page-context-root-id"))
        }
    }

    @Test
    void testAccessingNonExistentItemInTheRootContext() {
        setBodyHtml("""
            <div id="some-div-id">
                <div id="page-context-root-id">
                    <div id="item-id" class="item-class">text</div>
                </div>
            </div>
        """)
        def pageObject = createPageObject RootContextAwarePageObject
        assertThrows(NoSuchElementException, {
            // should throw an exception as it's located outside of the context
            pageObject.someDiv.isDisplayed()
        })
    }

    @Test
    void testRootContextUsesFinByForNonExistentElement() {
        setBodyHtml("")
        assertThrows(TimeoutException, {
            // has to throw timeout exception because getRootElement()
            // is generating NoSuchElementException which has to be ignored
            createPageObject RootContextAwarePageObject
        })
    }

    @Test
    void testTimeOfWaitingForTheTheRootContext() {
        setBodyHtml("")
        long msBefore = System.currentTimeMillis()
        setBodyHtml("""
            <div id="some-div-id">
                <div id="page-context-root-id">
                    <div id="item-id" class="item-class">text</div>
                </div>
            </div>
        """, HALF_A_PAGEOBJECT_WAIT_TIMEOUT_MS)
        def pageObject = createPageObject RootContextAwarePageObject
        long took = System.currentTimeMillis() - msBefore
        Assert.assertTrue(took >= HALF_A_PAGEOBJECT_WAIT_TIMEOUT_MS, "took $took;")
        Assert.assertTrue(pageObject.getRootElement().isDisplayed())
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class AccessingFieldsOnAvailability extends PageObject {
        @FindBy(id="some-div-id")
        WebElement someDiv

        @Override
        boolean isAvailable() {
            someDiv.isDisplayed()
        }
    }

    @Test
    void testAccessingYetNotWiredFieldsOnAvailabilityCheck() {
        setBodyHtml("""
            <div id="some-div-id">
            </div>
        """)
        assertThrows(NullPointerException, {
            // should throw NPE as the field is accessed from isAvailable() where it's not yet initialized
            createPageObject AccessingFieldsOnAvailability
        })
    }
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    @Test
    void testAvailabilityCheck() {
        setBodyHtml("")
        def builder = createPageObjectBuilder()

        Assert.assertFalse(builder.isAvailable(new PageObject() {
            @Override
            boolean isAvailable() {
                throw new NoSuchElementException("") // ignored exception is thrown
            }
        }))

        def expectedException = new IllegalArgumentException()
        assertReThrows(expectedException, {
            builder.isAvailable(new PageObject() {
                @Override
                boolean isAvailable() {
                    throw expectedException // non-ignored exception is thrown
                }
            })
        })

        Assert.assertTrue(builder.isAvailable(new PageObject() {
            @Override
            boolean isAvailable() {
                true
            }
        }))

        Assert.assertFalse(builder.isAvailable(new PageObject() {
            @Override
            boolean isAvailable() {
                false
            }
        }))
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class InitAwarePageObject extends PageObject {
        @FindBy(id="some-div-id")
        WebElement someDiv

        @Override
        void init() {
            Assert.assertTrue(someDiv.isDisplayed())
        }
    }

    @Test
    void testInitCanAccessWiredFields() {
        setBodyHtml("""
            <div id="some-div-id">
            </div>
        """)
        createPageObject InitAwarePageObject
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class PageObjectWithWebElementField extends PageObject {

        WebElement el

        @IgnoreBinding
        WebElement elIgnored

    }

    @Test
    void testIgnoreBindingElementLocatorFactory() {
        setBodyHtml("")
        WebDriverWait wait = new ExtendedWebDriverWait(webDriver, PAGEOBJECT_WAIT_TIMEOUT_SEC, PAGEOBJECT_POLLING_PERIOD_MS)
        def builder = new PageObjectBuilder(webDriver, wait, IgnoreBindingAwareElementLocatorFactory)
        def po = builder.createPageObject(PageObjectWithWebElementField)
        Assert.assertNotNull(po.el)
        Assert.assertNull(po.elIgnored)
    }

    @Test
    void testDefaultElementLocatorFactory() {
        setBodyHtml("")
        WebDriverWait wait = new ExtendedWebDriverWait(webDriver, PAGEOBJECT_WAIT_TIMEOUT_SEC, PAGEOBJECT_POLLING_PERIOD_MS)
        def builder = new PageObjectBuilder(webDriver, wait, DefaultElementLocatorFactory)
        def po = builder.createPageObject(PageObjectWithWebElementField)
        Assert.assertNotNull(po.el)
        Assert.assertNotNull(po.elIgnored)
    }

    @Test
    void testNoDefaultBindingElementLocatorFactory() {
        setBodyHtml("")
        WebDriverWait wait = new ExtendedWebDriverWait(webDriver, PAGEOBJECT_WAIT_TIMEOUT_SEC, PAGEOBJECT_POLLING_PERIOD_MS)
        def builder = new PageObjectBuilder(webDriver, wait, NoDefaultBindingLocatorFactory)
        def po = builder.createPageObject(PageObjectWithWebElementField)
        Assert.assertNull(po.el)
        Assert.assertNull(po.elIgnored)
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    static class ExtendedWiringPageObject extends PageObject {
        @FindBy(id="some-div-id")
        WebElement someDiv

        @FindBy(id="some-div-id")
        ExtendedWebElement someDivExtended
    }

    @Test
    void testWiringExtendedWebElement() {
        setBodyHtml("""
            <div id="some-div-id"/>
        """)
        def po = createPageObject ExtendedWiringPageObject
        Assert.assertNotNull(po.someDiv)
        Assert.assertNull(po.someDivExtended) // extended can't be wired
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    @Test
    void testSimpleJS() {
        setBodyHtml("")
        def po = createPageObject SimpleDynamicPageObject
        Assert.assertEquals(po.js.greet("Ranbir"), "hello, Ranbir")
    }

    @Test
    void testNoJSSources() {
        setBodyHtml("")
        assertThrows(IllegalStateException, {
            createPageObject PageObjectWithoutJSSources
        })
    }

    @Test
    void testExtension() {
        setBodyHtml("")
        def po = createPageObject ExtendedDynamicPageObject
        Assert.assertEquals(po.js.greet("Ranbir"), "hello, Ranbir")
        Assert.assertEquals(po.js.adieu("Ranbir"), "adieu, Ranbir")
    }

    @Test
    void testExtensionWithOverriding() {
        setBodyHtml("")
        def base = createPageObject AnnotatedPageObject
        Assert.assertEquals(base.js.getFive(), 5L)
        Assert.assertTrue(AnnotatedPageObject.isAssignableFrom(ExtendedPageObjectWithOverridingJSMethod))
        def extended = createPageObject ExtendedPageObjectWithOverridingJSMethod
        Assert.assertEquals(extended.js.getFive(), 6L)
        Assert.assertEquals(extended.js.getSix(), 6L)
    }

    @Test
    void testExtensionWithGroovyDelegate() {
        setBodyHtml("")
        def po = createPageObject DelegatingExtendedPageObject
        Assert.assertEquals(po.getFive(), 5L)
        Assert.assertEquals(po.getSix(), 6L)
    }

    @Test
    void testExtensionWithDelegateAndInjection() {
        setBodyHtml("")
        def po = createPageObject DelegatingExtendedWithInjection
        Assert.assertEquals(po.getFive(), 5L)
        Assert.assertEquals(po.getSix(), 6L)
    }

    @Test
    void testExtensionWithJSPropsConflicts() {
        setBodyHtml("")
        assertThrows(TimeoutException, {
            createPageObject ExtendedWithOverridingProp
        })
    }

    @Test
    void testAnnotatingPageObject() {
        setBodyHtml("")
        def po = createPageObject AnnotatedPageObject
        Assert.assertEquals(po.js.getFive(), 5L)
    }

    @Test
    void testArgumentsPassing() {
        setBodyHtml("")
        def po = createPageObject AnnotatedPageObject
        Assert.assertEquals(po.js.sum(3, 1), 4L)
    }

    @Test
    void testArgumentsAccessing() {
        setBodyHtml("")
        def po = createPageObject AnnotatedPageObject
        Assert.assertEquals(po.js.getArgs0(3), 3L)
    }


    @Test
    void testAnnotatedWithAllInline() {
        setBodyHtml("")
        def po = createPageObject AnnotatedPageObjectWithInnerJsInline
        Assert.assertEquals(po.getFive(), 5L)
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    /**
     * Creates page object instance with builder specifically preconfigured for the tests.
     *
     * @param pageClass the page object class
     * @return the created instance of the page object
     */
    private <T> T createPageObject(Class<T> pageClass) {
        // set page object waiter to 1 sec timeout with 300 ms polling interval
        PageObjectBuilder pageObjectBuilder = createPageObjectBuilder()
        pageObjectBuilder.createPageObject(pageClass)
    }

    /**
     * Creates a page object builder.
     *
     * @return the builder instance
     */
    private PageObjectBuilder createPageObjectBuilder() {
        WebDriverWait wait = new ExtendedWebDriverWait(webDriver, PAGEOBJECT_WAIT_TIMEOUT_SEC, PAGEOBJECT_POLLING_PERIOD_MS)
        def pageObjectBuilder = new PageObjectBuilder(webDriver, wait, IgnoreBindingAwareElementLocatorFactory)
        pageObjectBuilder
    }

}
