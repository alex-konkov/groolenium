package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface
import org.groolenium.framework.page.PageObject

@JavascriptInterface(AnnotatedPageObjectJSInterface)
class AnnotatedPageObject extends PageObject {

    def getJs() {
        super.<AnnotatedPageObjectJSInterface>getJsProxy()
    }

}
