package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface

@JavascriptInterface(DelegatingExtendedPageObjectJS)
class DelegatingExtendedPageObject extends DelegatingBasePageObject {
    @Delegate
    private DelegatingExtendedPageObjectJS js

    @Override
    void init() {
        super.init()
        js = super.<DelegatingExtendedPageObjectJS>getJsProxy()
    }
}
