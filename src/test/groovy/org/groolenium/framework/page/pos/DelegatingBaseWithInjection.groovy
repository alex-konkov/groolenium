package org.groolenium.framework.page.pos

import org.groolenium.framework.page.InjectJsProxy
import org.groolenium.framework.page.JavascriptImpl
import org.groolenium.framework.page.JavascriptInterface
import org.groolenium.framework.page.PageObject

@JavascriptInterface(JS)
class DelegatingBaseWithInjection extends PageObject {

    @Delegate
    @InjectJsProxy
    private JS js

    @JavascriptImpl(mode = JavascriptImpl.Mode.INLINE, js = "{getFive:function(){return 5;}}")
    static interface JS {
        long getFive()
    }

}
