package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface
import org.groolenium.framework.page.PageObject

@JavascriptInterface(SimpleDynamicPageObjectJS)
class SimpleDynamicPageObject extends PageObject {

    boolean isAvailable() {
        // remote methods are available in here
        js.greet("Ranbir") == "hello, Ranbir"
    }

    def getJs() {
        super.<SimpleDynamicPageObjectJS>getJsProxy()
    }

}
