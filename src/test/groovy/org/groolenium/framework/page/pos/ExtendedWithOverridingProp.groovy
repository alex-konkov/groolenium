package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface

@JavascriptInterface(ExtendedWithOverridingPropJS)
class ExtendedWithOverridingProp extends SimpleDynamicPageObject {

    def getJs() {
        super.<ExtendedWithOverridingPropJS>getJsProxy()
    }

}
