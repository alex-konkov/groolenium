package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(js = "/org/groolenium/framework/page/pos/ExtendedDynamicPageObject.js")
interface ExtendedDynamicPageObjectJS extends SimpleDynamicPageObjectJS {
    String adieu(String name)
}