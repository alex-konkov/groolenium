package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface

@JavascriptInterface(OverridingAnnotatedJSInterface)
class ExtendedPageObjectWithOverridingJSMethod extends AnnotatedPageObject {

    def getJs() {
        super.<OverridingAnnotatedJSInterface>getJsProxy()
    }

}
