package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(
    mode = JavascriptImpl.Mode.INLINE,
    js = """
        (function () {
            return {
                getSix: function() {
                    return 6;
                }
            }
        })();"""
)
interface DelegatingExtendedPageObjectJS extends DelegatingBasePageObjectJS {
    long getSix()
}
