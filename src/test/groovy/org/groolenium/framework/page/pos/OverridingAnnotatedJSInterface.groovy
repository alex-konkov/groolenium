package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(
    mode = JavascriptImpl.Mode.INLINE,
    js = """
        (function () {
            return {
                getFive: function() {
                    return 6;
                },
                getSix: function() {
                    return 6;
                }
            }
        })();"""
)
interface OverridingAnnotatedJSInterface extends AnnotatedPageObjectJSInterface {
    long getSix()
}