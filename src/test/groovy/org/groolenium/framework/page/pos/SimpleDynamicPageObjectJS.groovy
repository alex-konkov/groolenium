package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(js = "/org/groolenium/framework/page/pos/SimpleDynamicPageObject.js")
interface SimpleDynamicPageObjectJS {
    String greet(String name)
}