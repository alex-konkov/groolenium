package org.groolenium.framework.page.pos

import org.groolenium.framework.page.PageObject

class DelegatingBasePageObject extends PageObject {
    @Delegate
    private DelegatingBasePageObjectJS js

    @Override
    void init() {
        super.init()
        js = super.<DelegatingBasePageObjectJS>getJsProxy()
    }
}
