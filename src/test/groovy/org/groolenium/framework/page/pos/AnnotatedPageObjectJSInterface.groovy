package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(
    mode = JavascriptImpl.Mode.INLINE,
    js = """
        (function () {
            return {
                getFive: function() {
                    return 5;
                },
                sum: function(a, b) {
                    return a + b;
                },
                getArgs0: function() {
                    return arguments[0];
                }
            }
        })();""")
interface AnnotatedPageObjectJSInterface {
    long getFive()
    long sum(long arg1, long arg2)
    long getArgs0(long arg1)
}