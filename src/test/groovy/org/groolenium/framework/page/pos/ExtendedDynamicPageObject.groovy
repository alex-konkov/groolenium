package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface

@JavascriptInterface(ExtendedDynamicPageObjectJS)
class ExtendedDynamicPageObject extends SimpleDynamicPageObject {

    def getJs() {
        super.<ExtendedDynamicPageObjectJS>getJsProxy()
    }

}
