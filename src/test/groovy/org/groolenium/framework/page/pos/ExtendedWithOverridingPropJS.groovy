package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(js = "/org/groolenium/framework/page/pos/ExtendedWithOverridingProp.js")
public interface ExtendedWithOverridingPropJS extends SimpleDynamicPageObjectJS {
}