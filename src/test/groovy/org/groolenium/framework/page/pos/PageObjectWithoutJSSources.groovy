package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptInterface
import org.groolenium.framework.page.PageObject

@JavascriptInterface(PageObjectWithoutJSSourcesJS)
class PageObjectWithoutJSSources extends PageObject {
}
