package org.groolenium.framework.page.pos

import org.groolenium.framework.page.InjectJsProxy
import org.groolenium.framework.page.JavascriptImpl
import org.groolenium.framework.page.JavascriptInterface

@JavascriptInterface(JS)
class DelegatingExtendedWithInjection extends DelegatingBaseWithInjection {

    @Delegate
    @InjectJsProxy
    private JS js

    @JavascriptImpl(mode = JavascriptImpl.Mode.INLINE, js = "{getSix:function(){return 6;}}")
    static interface JS extends DelegatingBaseWithInjection.JS {
        long getSix()
    }

}
