package org.groolenium.framework.page.pos

import org.groolenium.framework.page.JavascriptImpl

@JavascriptImpl(
    mode = JavascriptImpl.Mode.INLINE,
    js = """
        (function () {
            return {
                getFive: function() {
                return 5;
            }
        }})();"""
)
interface DelegatingBasePageObjectJS {
    long getFive()
}