package org.groolenium.framework

import org.groolenium.framework.page.ExtendedDefaultElementLocatorFactory
import org.groolenium.framework.page.FindByJQuery
import org.groolenium.framework.page.PageObject
import org.groolenium.framework.page.PageObjectBuilder
import org.groolenium.framework.webdriver.ExtendedWebDriverWait
import org.groolenium.framework.webdriver.by.ByJQuery
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.CacheLookup
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.Assert
import org.testng.annotations.Test

/**
 * Tests for JQuery elements finding.
 *
 * @author Alex K
 */
class ByJQueryTest extends WebDriverBackedTest {

    private static final String HTML_BODY = """
           <div id="outer-div-id" class="outer-div">
                <div id="mid-div-id" class="mid-div">
                    <div id="inner-div1-id" class="inner-div">text1</div>
                    <div id="inner-div2-id" class="inner-div">text2</div>
                </div>
            </div>
        """

    @Test
    void testFindGloballyExistingOne() {
        bringPageWithJQuery()
        Assert.assertEquals(
                webDriver.findElement(ByJQuery.selector('#inner-div1-id')).getAttribute('class'),
                'inner-div'
        )
    }

    @Test
    void testFindGloballyExistingOnes() {
        bringPageWithJQuery()
        Assert.assertEquals(
                webDriver.findElements(ByJQuery.selector('.inner-div')).size(),
                2
        )
    }

    @Test
    void testFindGloballyNonExistentOne() {
        bringPageWithJQuery()
        // find globally non-existing one
        assertThrows(NoSuchElementException, {
            webDriver.findElement(ByJQuery.selector('#non-existent'))
        })
    }

    @Test
    void testFindGloballyNonExistentOnes() {
        bringPageWithJQuery()
        // find globally non-existing ones
        Assert.assertEquals(
                webDriver.findElements(ByJQuery.selector('#non-existent')).size(),
                0
        )
    }

    @Test
    void testFindInContextExistingOne() {
        bringPageWithJQuery()
        def searchCtx = webDriver.findElement(ByJQuery.selector('#mid-div-id'))

        // find in context existing one
        Assert.assertEquals(
                searchCtx.findElement(ByJQuery.selector('#inner-div1-id')).getAttribute('class'),
                'inner-div'
        )
    }

    @Test
    void testFindInContextExistingOnes() {
        bringPageWithJQuery()
        def searchCtx = webDriver.findElement(ByJQuery.selector('#mid-div-id'))

        // find in context existing ones
        Assert.assertEquals(
                searchCtx.findElements(ByJQuery.selector('.inner-div')).size(),
                2
        )
    }

    @Test
    void testFindInContextGloballyNonExistingOne() {
        bringPageWithJQuery()
        def searchCtx = webDriver.findElement(ByJQuery.selector('#mid-div-id'))

        // find in context globally non-existing one
        assertThrows(NoSuchElementException, {
            searchCtx.findElement(ByJQuery.selector('#non-existent'))
        })

    }

    @Test
    void testFindInContextGloballyNonExistingOnes() {
        bringPageWithJQuery()
        def searchCtx = webDriver.findElement(ByJQuery.selector('#mid-div-id'))

        // find in context globally non-existing ones
        Assert.assertEquals(
                searchCtx.findElements(ByJQuery.selector('#non-existent')).size(),
                0
        )

    }

    @Test
    void testFindInContextOneWhichIsOutsideOf() {
        bringPageWithJQuery()
        def searchCtx = webDriver.findElement(ByJQuery.selector('#mid-div-id'))

        // find in context element outside of it
        assertThrows(NoSuchElementException, {
            searchCtx.findElement(ByJQuery.selector('#outer-div-id'))
        })

    }

    @Test
    void testFindInContextOnesWhichAreOutsideOf() {
        bringPageWithJQuery()
        def searchCtx = webDriver.findElement(ByJQuery.selector('#mid-div-id'))

        // find in context elements outside of it
        Assert.assertEquals(
                searchCtx.findElements(ByJQuery.selector('#outer-div-id')).size(),
                0
        )
    }

    @Test
    void testWithSingleQuotes() {
        bringPageWithJQuery()

        // single quotes
        Assert.assertEquals(
                webDriver.findElements(ByJQuery.selector("div[class^='inner']")).size(),
                2
        )

    }

    @Test
    void testWithDoubleQuotes() {
        bringPageWithJQuery()

        // double quotes
        Assert.assertEquals(
                webDriver.findElements(ByJQuery.selector('div[class^="inner"]')).size(),
                2
        )

    }

    @Test
    void testWithSingleAndDoubleQuotes() {
        bringPageWithJQuery()

        // single and double quotes
        assertThrows(IllegalArgumentException, {
            ByJQuery.selector('''div[class^="John"]:contains('Doe')''')
        })

    }

    @Test
    void testWithNoJQuery() {
        assertThrows(IllegalStateException, {
            webDriver.findElements(ByJQuery.selector('#some-div-id'))
        })
    }

    static class PageObjectWithJQueryField extends PageObject {
        @FindByJQuery(".mid-div")
        WebElement midDiv
    }

    @Test
    void testWiringOfEl() {
        bringPageWithJQuery()
        PageObjectBuilder builder = createBuilder()
        PageObjectWithJQueryField po = builder.createPageObject(PageObjectWithJQueryField)
        Assert.assertEquals(po.midDiv.getAttribute("id"), "mid-div-id");
    }

    @Test
    void testCachingOfNonCachedEl() {
        bringPageWithJQuery()
        PageObjectBuilder builder = createBuilder()
        PageObjectWithJQueryField po = builder.createPageObject(PageObjectWithJQueryField)
        Assert.assertEquals(po.midDiv.getAttribute("id"), "mid-div-id")
        setBodyHtml ''
        assertThrowsInstanceOf(NoSuchElementException, { // lookup is made again
            po.midDiv.getAttribute("id")
        })
    }

    static class PageObjectWithCachedJQueryField extends PageObject {
        @CacheLookup
        @FindByJQuery(".mid-div")
        WebElement midDiv
    }

    @Test
    void testCachingOfCachedEl() {
        bringPageWithJQuery()
        PageObjectBuilder builder = createBuilder()
        PageObjectWithCachedJQueryField po = builder.createPageObject(PageObjectWithCachedJQueryField)
        Assert.assertEquals(po.midDiv.getAttribute("id"), "mid-div-id");
        setBodyHtml ''
        assertThrowsInstanceOf(StaleElementReferenceException, { // no lookup is made again => stale
            po.midDiv.getAttribute("id")
        })
    }

    static class PageObjectWithJQueryFieldList extends PageObject {
        @FindByJQuery("div")
        List divs
    }

    @Test
    void testWiringOfElsList() {
        bringPageWithJQuery()
        PageObjectBuilder builder = createBuilder()
        PageObjectWithJQueryFieldList po = builder.createPageObject(PageObjectWithJQueryFieldList)
        Assert.assertEquals(po.divs.size(), 4)
    }

    @Test
    void testCachingOfNotCachedElsList() {
        bringPageWithJQuery()
        PageObjectBuilder builder = createBuilder()
        PageObjectWithJQueryFieldList po = builder.createPageObject(PageObjectWithJQueryFieldList)
        Assert.assertEquals(po.divs.size(), 4)
        setBodyHtml ''
        Assert.assertEquals(po.divs.size(), 0) // lookup is made again
    }

    static class PageObjectWithCachedJQueryFieldList extends PageObject {
        @CacheLookup
        @FindByJQuery("div")
        List divs
    }

    @Test
    void testCachingOfCachedElsList() {
        bringPageWithJQuery()
        PageObjectBuilder builder = createBuilder()
        PageObjectWithCachedJQueryFieldList po = builder.createPageObject(PageObjectWithCachedJQueryFieldList)
        Assert.assertEquals(po.divs.size(), 4)
        setBodyHtml ''
        Assert.assertEquals(po.divs.size(), 4) // cached => same list
        assertThrowsInstanceOf(StaleElementReferenceException, { // must be stale when accessed
            ((WebElement)po.divs[0]).getAttribute('id')
        })
    }

    private void bringPageWithJQuery() {
        goToPageWithJQuery()
        setBodyHtml(HTML_BODY)
    }

    private void goToPageWithJQuery() {
        URL page = ByJQueryTest.class.classLoader.getResource('blank.html')
        webDriver.navigate().to(page)
        String jQueryCode = ByJQueryTest.class.classLoader.getResourceAsStream('jquery-1.9.0.min.js').text
        webDriver.executeScript(jQueryCode)
    }

    private PageObjectBuilder createBuilder() {
        WebDriverWait wait = new ExtendedWebDriverWait(webDriver, 5, 1000)
        PageObjectBuilder builder = new PageObjectBuilder(webDriver, wait, ExtendedDefaultElementLocatorFactory)
        builder
    }

}
