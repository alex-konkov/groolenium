package org.groolenium.framework

import org.groolenium.framework.webdriver.ExtendedWebElement
import org.openqa.selenium.By
import org.testng.Assert
import org.testng.annotations.Test

/**
 * Various test for WebDriver extension.
 *
 * @author Alex K
 */
class ExtendedWebDriverTest extends WebDriverBackedTest {

    private static final String HTML_BODY = """
           <div id="outer-div-id" class="outer-div">
                <div id="mid-div-id" class="mid-div">
                    <div id="inner-div1-id" class="inner-div">text1</div>
                    <div id="inner-div2-id" class="inner-div">text2</div>
                </div>
            </div>
        """

    @Test
    void testWebElementWrapping() {
        bringPageBody()
        Assert.assertEquals(webDriver.findElement(By.className('inner-div')).class, ExtendedWebElement)
        webDriver.findElements(By.className('inner-div')).each {
            Assert.assertTrue(it instanceof  ExtendedWebElement)
        }
        def topEl = webDriver.findElement(By.id('outer-div-id'))
        Assert.assertEquals(topEl.findElement(By.className('inner-div')).class, ExtendedWebElement)
        topEl.findElements(By.className('inner-div')).each {
            Assert.assertTrue(it instanceof  ExtendedWebElement)
        }
    }

    private void bringPageBody() {
        setBodyHtml(HTML_BODY)
    }

}
