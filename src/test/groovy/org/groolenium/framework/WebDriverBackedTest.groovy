package org.groolenium.framework

import org.groolenium.framework.webdriver.ExtendedWebDriver
import org.groolenium.framework.webdriver.ExtendedWebDriverAware
import org.testng.Assert
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod

/**
 * UI test class which is aware of the underlying WebDriver instance.
 *
 * @author Alex K
 */
abstract class WebDriverBackedTest implements ExtendedWebDriverAware {

    ExtendedWebDriver webDriver

    @BeforeMethod
    void initWebDriver() {
        webDriver = ExtendedWebDriverBuilder.getLocal()
    }

    @AfterMethod
    void shutdownWebDriver() {
        webDriver.quit()
    }

    /**
     * Executes the given code and validates that it throws exception of exactly the given type.
     *
     * @param exceptionClass the type of expected exception
     * @param code the code which is expected to throw an exception
     */
    protected static <T extends Exception> void assertThrows(Class<T> exceptionClass, Closure code) {
        try {
            code.call()
            Assert.fail("exception expected: ${exceptionClass.simpleName}")
        } catch (Exception e) {
            boolean wasThrown = e.getClass().equals(exceptionClass) // exact type expected
            Assert.assertTrue(wasThrown, "expected exception ${exceptionClass.simpleName} hasn't been thrown")
        }
    }

    /**
     * Executes the given code and validates that it throws exception of the given type, allowing subclasses.
     *
     * @param exceptionClass the type of expected exception, allowing subclasses
     * @param code the code which is expected to throw an exception
     */
    protected static <T extends Exception> void assertThrowsInstanceOf(Class<T> exceptionClass, Closure code) {
        try {
            code.call()
            Assert.fail("exception expected: ${exceptionClass.simpleName}")
        } catch (Exception e) {
            boolean wasThrown = exceptionClass.isAssignableFrom(e.getClass()) // subclasses are allowed
            Assert.assertTrue(wasThrown, "expected exception ${exceptionClass.simpleName} hasn't been thrown")
        }
    }

    /**
     * Executes the given code and validates that it throws the given exception instance.
     *
     * @param expectedException the exception expected to be thrown
     * @param code the code which is expected to throw the exception
     */
    protected static void assertReThrows(Exception expectedException, Closure code) {
        try {
            code.call()
            Assert.fail("exception expected: ${expectedException.class.simpleName}")
        } catch (Exception e) {
            boolean wasThrown = e.is(expectedException)
            Assert.assertTrue(wasThrown, "expected exception ${expectedException.class.simpleName} hasn't been thrown")
        }
    }

    /**
     * Sets the given HTML into the document body.
     *
     * @param html the inner HTML string to set into the document body
     * @param executeInMillis the amount of time in milliseconds to set document body in
     */
    protected void setBodyHtml(String html, int executeInMillis = 0) {
        html = html.trim().replaceAll('\n', '') // obfuscating it or else it doesn't work for multiline htmls
        def js = "document.body.innerHTML='$html';"
        if (executeInMillis > 0) {
            js = "setTimeout(function(){$js},$executeInMillis)"
        }
        webDriver.executeScript(js)
    }

}

