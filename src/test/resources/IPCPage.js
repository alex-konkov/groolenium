(function () {
    return {
        blah: 7,

        getFive: function() {
            return 5;
        },

        getBlah: function() {
            return this.blah;
        },

        getEchoed: function(a) {
            return a;
        }
    }
})()