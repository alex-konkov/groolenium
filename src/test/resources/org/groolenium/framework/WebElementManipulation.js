(function() {
    return {
        getDocument: function() {
            return window.document.documentElement;
        },
        addDivTo: function(el) {                      /* <html></html> */
            var div = document.createElement("div");
            div.innerHTML = "added";                  /* <div>added</div> */
            el.appendChild(div);
            return div;                               /* <html><div>added</div></html> */
        }
    }
})();
