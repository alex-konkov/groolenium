package org.groolenium.framework.page

import org.groolenium.framework.Configuration
import org.groolenium.framework.webdriver.ExtendedWebDriver
import org.groolenium.framework.webdriver.ExtendedWebDriverWait
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory

/**
 * Configured page object builder. Configuration example:
 *
 * <p>
 * {@code
 * webDriverFramework.pageObjectBuilder.timeoutSec = 30
 * webDriverFramework.pageObjectBuilder.pollingPeriodMs = 1000
 * webDriverFramework.pageObjectBuilder.elementLocatorFactory = org.groolenium.framework.page.NoDefaultBindingLocatorFactory
 * }
 *
 * @author Alex K
 */
class ConfiguredPageObjectBuilder {

    private ExtendedWebDriver webDriver

    ConfiguredPageObjectBuilder(ExtendedWebDriver webDriver) {
        this.webDriver = webDriver
    }

    PageObjectBuilder build() {
        long timeoutSec = webDriver.longWait.timeoutSec
        long pollingPeriodMs = webDriver.longWait.sleepBetweenPollsMs
        def factoryClassValue = getConfig()['elementLocatorFactory']
        Class factoryClass = Configuration.toClass(factoryClassValue)
        assert factoryClass, "Couldn't find correct factory value"
        assert ElementLocatorFactory.isAssignableFrom(factoryClass), "not an ElementLocatorFactory"
        ExtendedWebDriverWait wait = webDriver.getWait(timeoutSec, pollingPeriodMs)
        new PageObjectBuilder(webDriver, wait, factoryClass)
    }

    private static def getConfig() {
        Configuration.get()['webDriverFramework']['pageObjectBuilder']
    }

}
