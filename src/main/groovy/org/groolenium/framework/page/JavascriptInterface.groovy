package org.groolenium.framework.page

import java.lang.annotation.*

/**
 * Annotation for defining Javascript interface in the page object class.
 *
 * @author Alex K
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JavascriptInterface {
    Class<?> value()
}