package org.groolenium.framework.page

import java.lang.annotation.*

/**
 * Annotation for marking fields in {@link PageObject} implementations as not requiring automatic binding by
 * {@link org.openqa.selenium.support.PageFactory}. Page objects which make use of this annotation have to be
 * initialized by {@link PageObjectBuilder} configured to use {@link IgnoreBindingAwareElementLocatorFactory}
 * locator factory.
 *
 *
 * @author Alex K
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface IgnoreBinding {

    // No body

}
