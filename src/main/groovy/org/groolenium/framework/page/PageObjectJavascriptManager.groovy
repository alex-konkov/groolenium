package org.groolenium.framework.page

import java.lang.reflect.Field
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method
import java.lang.reflect.Proxy

/**
 * Utility class for working with page object's Javascript implementation.
 *
 * @author Alex K
 */
class PageObjectJavascriptManager {

    private static final String EXTEND_FUNC_NAME = "extend_pageobject_js"

    private PageObject page

    /**
     * Constructs new instance for the provided page object.
     *
     * @param page the page object for managing Javascript capabilities
     */
    PageObjectJavascriptManager(PageObject page) {
        this.page = page
    }

    /**
     * Initializes proxy object for remote invocation of the Javascript methods, if there's an interface declared.
     * On the Javascript side, it registers the declared functions which become available for further invocation
     * through the Javascript proxy object of the page object.
     *
     * @param page the page object to initialize Javascript proxy object for
     */
    void initJsProxy() {
        if (!hasJsInterface()) {
            return
        }
        Class jsInterface = getJsInterface();
        getInterfaceHierarchyChain(jsInterface).each {
            initInterface(it)
        }
        validateFunctions()
        def proxy = createJsProxy()
        page.setJsProxy(proxy)
        injectProxyRef()
    }

    /**
     * Injects proxy object into page object's fields marked using {@link InjectJsProxy} annotation.
     */
    private void injectProxyRef() {
        def superCl = page.class
        while (superCl) {
            superCl.declaredFields.findAll {
                it.annotations.any { it.annotationType() == InjectJsProxy }
            }.each { Field field ->
                if (!field.type.isAssignableFrom(page.jsProxy.class)) {
                    throw new IllegalStateException("Field '${field.name}' has incompatible type for injection")
                }
                if (!field.accessible) {
                    field.accessible = true
                }
                field.set(page, page.jsProxy)
            }
            superCl = superCl.superclass
        }
    }

    /**
     * Gets all the interfaces which the provided interface is in the "is a" relationship with, including
     * the given interface itself.
     *
     * @param clazz the interface class to build an interfaces set for
     * @return the interfaces, starting from the base one
     */
    private static Collection<Class> getInterfaceHierarchyChain(Class<?> clazz) {
        Set results = new LinkedHashSet()
        clazz.interfaces.each {
            results.addAll(getInterfaceHierarchyChain(it))
            results.add(it)
        }
        results.add(clazz)
        results
    }

    /**
     * Gets Javascript text which has to contain an implementation of the given interface class.
     *
     * @param jsInterface the interface class to get Javascript text for
     * @return the Javascript source text
     */
    private static String getJsImplText(Class<?> jsInterface) {
        def annotated = jsInterface.getAnnotation(JavascriptImpl)
        if (annotated) {
            if (annotated.mode() == JavascriptImpl.Mode.INLINE) { // JS code is provided inline
                return annotated.js()
            } else if (annotated.mode() == JavascriptImpl.Mode.RESOURCE) { // JS is provided in the resource
                def jsResourceUrl = jsInterface.getResource(annotated.js())
                if (!jsResourceUrl) {
                    throw new IllegalStateException("Unable to find Javascript resource '${annotated.js()}'")
                }
                return jsResourceUrl.text
            } else {
                throw new IllegalArgumentException("Unsupported Javascript implementation mode");
            }
        } else {
            throw new IllegalStateException("${jsInterface.name} must specify JS implementation, please use annotation")
        }
    }

    /**
     * Applies Javascript implementation represented by the given interface to the page object.
     *
     * @param interfaceClass the interface class which describes Javascript interface
     */
    private void initInterface(Class<?> interfaceClass) {
        String jsText = getJsImplText(interfaceClass)
        if (!page.webDriver.executeScriptReturn("typeof(window['${EXTEND_FUNC_NAME}'])==='function'")) {
            page.webDriver.executeScript("""
                window['${EXTEND_FUNC_NAME}'] = function(extendee, extender) {
                    var newProps = Object.getOwnPropertyNames(extender);
                    newProps.forEach(function(propName) {
                        var destination = Object.getOwnPropertyDescriptor(extender, propName);
                        Object.defineProperty(extendee, propName, destination);
                    });
                }
            """)
        }
        page.webDriver.executeScript("window['${page.id}']=window['${page.id}']||{};")
        page.webDriver.executeScript("var o=${jsText};window['${EXTEND_FUNC_NAME}'](window['${page.id}'],o);")
    }

    /**
     * Creates proxy object for remote methods invocation.
     *
     * @return the proxy instance
     */
    private Object createJsProxy() {
        def invoker = { object, Method method, Object[] args ->
            callRemoteJsMethod(method.name, args)
        } as InvocationHandler
        def jsInterface = getJsInterface()
        Proxy.newProxyInstance(jsInterface.classLoader, [jsInterface] as Class[], invoker)
    }

    /**
     * Validates that each method in the Javascript interface has an implementation.
     *
     * @throws IllegalStateException if some of the declared methods are not implemented in Javascript
     */
    private void validateFunctions() {
        Class jsInterface = getJsInterface()
        jsInterface.methods.each { Method m ->
            if (!page.webDriver.executeScriptReturn("typeof(window['${page.id}']['${m.name}'])==='function'")) {
                throw new IllegalStateException("Method '${jsInterface.name}.${m.name}' has no Javascript implementation")
            }
        }
    }

    /**
     * Locates class representing Javascript interface for RMI. The class is a Java interface which is
     * identified by the {@link JavascriptInterface} annotation.
     *
     * @return the interface class representing Javascript interface, or null if nothing is declared
     */
    private Class<?> getJsInterface() {
        def annotated = page.class.getAnnotation(JavascriptInterface)
        if (!annotated) {
            return null
        } else if (annotated && !annotated.value().isInterface()) {
            throw new IllegalStateException("Javascript interface annotation must be given an interface class")
        }
        annotated.value() // annotation found => taking an interface from it
    }

    /**
     * Calls remote Javascript method with the given name and arguments through the WebDriver.
     *
     * @param name the method name
     * @param args the array with method arguments
     * @return the invocation result
     */
    private Object callRemoteJsMethod(String name, Object[] args) {
        page.webDriver.executeScriptReturn(
                "window['${page.id}']['${name}'].apply(window['${page.id}'],arguments)",
                args == null ? [] : args
        )
    }

    /**
     * Checks if the page object has remote Javascript interface declared.
     *
     * @return <tt>true</tt> if the given page object has Javascript interface declared, <tt>false</tt> otherwise
     */
    private boolean hasJsInterface() {
        getJsInterface() != null
    }

}
