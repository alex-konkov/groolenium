package org.groolenium.framework.page

import org.groolenium.framework.webdriver.ExtendedWebElement
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator
import org.openqa.selenium.support.pagefactory.ElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory

import java.lang.reflect.Field

/**
 * Extended field decorator with additional capabilities.
 *
 * @author Alex K
 */
class ExtendedDefaultFieldDecorator extends DefaultFieldDecorator {

    ExtendedDefaultFieldDecorator(ElementLocatorFactory factory) {
        super(factory)
    }

    public Object decorate(ClassLoader loader, Field field) {
        if (field.type == ExtendedWebElement) { // no support of such type because of weird class cast exceptions
            return null
        }
        if (field.getAnnotation(FindByJQuery)) { // making JQuery wiring happen
            ElementLocator locator = factory.createLocator(field);
            if (locator == null) {
                return null
            }
            if (WebElement.class.isAssignableFrom(field.getType())) {
                return proxyForLocator(loader, locator);
            } else if (List.class.isAssignableFrom(field.getType())) {
                return proxyForListLocator(loader, locator);
            } else {
                return null
            }
        }
        super.decorate(loader, field) // default delegation
    }

}
