package org.groolenium.framework.page

import org.openqa.selenium.SearchContext
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.FindBys
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory
import org.openqa.selenium.support.pagefactory.ElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory

import java.lang.reflect.Field

/**
 * Locator factory which works the same way as Selenium's default one, but it has disabled
 * wiring by id or name when there's no explicit locator annotation present.
 *
 * @author Alex K
 * @see DefaultElementLocatorFactory
 */
class NoDefaultBindingLocatorFactory implements ElementLocatorFactory {

    private final ElementLocatorFactory factory;

    NoDefaultBindingLocatorFactory(SearchContext searchContext) {
        factory = new ExtendedDefaultElementLocatorFactory(searchContext);
    }

    ElementLocator createLocator(Field field) {
        if (shouldWire(field)) {
            return factory.createLocator(field)
        }
        null
    }

    /**
     * Checks if wiring should be done for the given field or not. The only fields to wire are
     * the ones which are annotated using {@link org.openqa.selenium.support.FindBy} or
     * {@link org.openqa.selenium.support.FindBys} or {@link FindByJQuery}.
     *
     * @param field the field to check
     * @return <tt>true</tt> if the given field has to be wired, <tt>false</tt> otherwise
     */
    private static boolean shouldWire(Field field) {
        if (field.getAnnotation(FindBy.class) ||
                field.getAnnotation(FindBys.class) ||
                field.getAnnotation(FindByJQuery.class)) {
            return true
        }
        false
    }

}
