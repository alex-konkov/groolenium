package org.groolenium.framework.page

import org.groolenium.framework.webdriver.ExtendedWebDriver
import org.groolenium.framework.webdriver.ExtendedWebDriverAware
import org.openqa.selenium.NotFoundException
import org.openqa.selenium.WebElement

/**
 * Base page object class which can represent either an entire page or some page fragment.
 *
 * @author Alex K
 */
abstract class PageObject implements ExtendedWebDriverAware {

    /**
     * A unique id of the page object.
     */
    private String id = UUID.randomUUID()

    /**
     * Property with WebDriver instance the page object will work through.
     */
    ExtendedWebDriver webDriver

    /**
     * Property for proxy object intended for remote Javascript method invocations.
     */
    Object jsProxy

    /**
     * Property holding a page object builder reference. It is useful when the page object has
     * to originate another page objects.
     */
    PageObjectBuilder builder

    /**
     * Gets the root element of the page object. Implementations may provide their own root element that will
     * be accounted by Selenium's {@link org.openqa.selenium.support.PageFactory} when locating elements.
     * For page objects without root elements the method has to return null.
     *
     * <p>Note that for the phase when the page object gets built, it's allowed for the method to throw
     * {@link org.openqa.selenium.NotFoundException} so there's no need to catch it explicitly. In such
     * case the builder understands it as the element is not yet present.
     *
     * @return the element for searching other elements in the page object
     */
    protected WebElement getRootElement() throws NotFoundException {
        null
    }

    /**
     * Availability criterion which stands for determining if the page object is up on a page at any point of time.
     * This is useful for dynamic objects which are aware of the state of their availability. One of the typical
     * usages is polling it until it becomes <tt>true</tt> which means that the page object is available.
     *
     * @return <tt>true</tt> is the page object is available, <tt>false</tt> otherwise
     */
    protected boolean isAvailable() {
        true
    }

    /**
     * Initialization method for the page object. Implementations may define their own init logic, if required.
     */
    protected void init() {
        // no operations
    }

    /**
     * Gets Javascript proxy object cast to T.
     *
     * @throws IllegalStateException if the proxy is not initialized
     * @return the proxy object
     */
    protected <T> T getJsProxy() {
        if (!jsProxy) {
            throw new IllegalStateException("JS proxy is not initialized for ${getClass().name}")
        }
        (T) jsProxy
    }

    /**
     * Returns a unique identifier of the page object.
     *
     * @return the page object id
     */
    String getId() {
        id
    }

}
