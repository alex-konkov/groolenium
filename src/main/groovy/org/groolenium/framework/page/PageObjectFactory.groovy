package org.groolenium.framework.page

/**
 * Factory which has to do with construction of the page object.
 *
 * @author Alex K
 */
interface PageObjectFactory {

    /**
     * Creates a new page object instance.
     *
     * @return the new page object instance
     */
    PageObject createInstance()
}
