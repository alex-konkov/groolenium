package org.groolenium.framework.page

import org.openqa.selenium.SearchContext
import org.openqa.selenium.support.pagefactory.DefaultElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory

import java.lang.reflect.Field

/**
 * Extension of the default element locator factory with additional features like JQuery, etc.
 *
 * @author Alex K
 */
class ExtendedDefaultElementLocatorFactory implements ElementLocatorFactory {

    private final SearchContext searchContext;

    public ExtendedDefaultElementLocatorFactory(SearchContext searchContext) {
        this.searchContext = searchContext;
    }

    public ElementLocator createLocator(Field field) {
        if (field.getAnnotation(FindByJQuery)) {
            return new JQueryElementLocator(searchContext, field)
        }
        new DefaultElementLocator(searchContext, field)
    }

}
