package org.groolenium.framework.page

import java.lang.annotation.*

/**
 * Annotation for defining Javascript implementation.
 *
 * @author Alex K
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface JavascriptImpl {

    /**
     * Denotes how {@link #js()} annotation attribute is going to be treated.
     *
     * @return the chosen mode
     */
    JavascriptImpl.Mode mode() default JavascriptImpl.Mode.RESOURCE

    /**
     * Refers to the provided Javascript implementation. When {@link #mode()}
     * is set to {@link JavascriptImpl.Mode#RESOURCE} the attribute value is a
     * name of the resource expected in the classpath. If {@link #mode()} is
     * {@link JavascriptImpl.Mode#INLINE} the Javascript snippet is expected
     * to be provided right in the attribute value.
     *
     * @return the Javascript implementation
     */
    String js()

    /**
     * Mode for the Javascript implementation.
     */
    enum Mode {

        /**
         * Shows that Javascript implementation is going to be provided inline.
         */
        INLINE,

        /**
         * Means that Javascript implementation is provided in the file resource.
         */
        RESOURCE

    }

}