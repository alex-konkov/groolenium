package org.groolenium.framework.page

import org.groolenium.framework.webdriver.ExtendedWebDriverWait

/**
 * Wait object for page object builder which adds more waiting methods needed for page objects builder.
 *
 * @author Alex K
 */
class PageObjectBuilderWaitWrapper {

    @Delegate
    private ExtendedWebDriverWait delegate

    private PageObjectBuilder builder

    PageObjectBuilderWaitWrapper(ExtendedWebDriverWait wait, PageObjectBuilder builder) {
        this.delegate = wait
        this.builder = builder
    }

    public void untilAvailable(PageObject pageObject) {
        until({
            builder.isAvailable(pageObject)
        }, "Failed to wait until page object '${pageObject.class.name}' is available")
    }

    public <T extends PageObject> void untilAvailable(Class<T> pageClass) {
        until({
            builder.isAvailable(pageClass.newInstance())
        }, "Failed to wait until page object '${pageClass.name}' is available")
    }

}
