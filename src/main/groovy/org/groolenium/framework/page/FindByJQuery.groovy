package org.groolenium.framework.page

import java.lang.annotation.Documented
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * Annotation for supporting JQuery locators.
 *
 * @author Alex K
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FindByJQuery {

    /**
     * Gets JQuery locator.
     *
     * @return the JQuery locator string
     */
    String value()

}