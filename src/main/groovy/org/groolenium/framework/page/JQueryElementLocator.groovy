package org.groolenium.framework.page

import org.groolenium.framework.webdriver.by.ByJQuery
import org.openqa.selenium.By
import org.openqa.selenium.SearchContext
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.CacheLookup
import org.openqa.selenium.support.pagefactory.ElementLocator

import java.lang.reflect.Field

/**
 * JQuery element locator.
 *
 * @author Alex K
 */
class JQueryElementLocator implements ElementLocator {

    private SearchContext searchContext;
    private boolean shouldCache;
    private By by;
    private WebElement cachedElement;
    private List<WebElement> cachedElementList;

    /**
     * Constructs the locator.
     *
     * @param searchContext the search context
     * @param field the locator field
     */
    JQueryElementLocator(SearchContext searchContext, Field field) {
        this.searchContext = searchContext;
        shouldCache = field.getAnnotation(CacheLookup.class) != null;
        FindByJQuery ann = field.getAnnotation(FindByJQuery)
        assert ann, "FindByJQuery annotation expected"
        by = ByJQuery.selector(ann.value())
    }

    /**
     * Finds the element.
     */
    @Override
    public WebElement findElement() {
        if (cachedElement != null && shouldCache) {
            return cachedElement;
        }
        WebElement element = searchContext.findElement(by);
        if (shouldCache) {
            cachedElement = element;
        }
        return element;
    }

    /**
     * Finds the elements list.
     */
    @Override
    public List<WebElement> findElements() {
        if (cachedElementList != null && shouldCache) {
            return cachedElementList;
        }
        List<WebElement> elements = searchContext.findElements(by);
        if (shouldCache) {
            cachedElementList = elements;
        }
        return elements;
    }

}
