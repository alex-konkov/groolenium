package org.groolenium.framework.page

import org.groolenium.framework.webdriver.ExtendedWebDriver
import org.groolenium.framework.webdriver.ExtendedWebDriverWait
import org.openqa.selenium.SearchContext
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory
import org.openqa.selenium.support.ui.WebDriverWait

/**
 * Builder which stands for constructing page objects and initializing them.
 *
 * <p>The initialization steps are the following:
 * <ul>
 *   <li> initializing remote Javascript interface
 *   <li> waiting for availability of the page object
 *   <li> waiting for availability of the page object's root element
 *   <li> initializing page object using Selenium API using {@link PageFactory}
 *   <li> calling 'init' method of the page object
 * </ul>
 *
 * @author Alex K
 * @see <a href="http://code.google.com/p/webdriver/wiki/PageObjects">Page Objects Wiki</a>
 */
public class PageObjectBuilder {

    /**
     * Extended WebDriver needed for the builder.
     */
    private ExtendedWebDriver webDriver

    /**
     * Wait object for the builder.
     */
    private PageObjectBuilderWaitWrapper wait

    /**
     * Element locator factory class which has to have a constructor taking search context parameter.
     */
    private Class<? extends ElementLocatorFactory> elementLocatorFactoryClass

    /**
     * Constructs new builder instance.
     *
     * @param webDriver the WebDriver instance to use
     * @param wait the wait configuration to use
     * @param elementLocatorFactory the element location factory to use
     */
    public PageObjectBuilder(ExtendedWebDriver webDriver, ExtendedWebDriverWait wait,
                                Class<? extends ElementLocatorFactory> elementLocatorFactory) {
        this.webDriver = webDriver
        this.wait = new PageObjectBuilderWaitWrapper(wait, this)
        this.elementLocatorFactoryClass = elementLocatorFactory
    }

    /**
     * Constructs a new initialized page object which is expected to have a no-arg constructor.
     *
     * @param pageClass the page object class with no-argument constructor
     * @return the new initialized page object
     */
    public <T extends PageObject> T createPageObject(Class<T> pageClass) {
        assert PageObject.isAssignableFrom(pageClass), "The given class is not a PageObject"
        // creating a new instance of the page object
        PageObject page = pageClass.newInstance()
        // initializing it
        initPageObject(page)
    }

    /**
     * Creates new instance of the page object using the given factory for instantiating it.
     *
     * @param factory the page object factory which has to instantiate it
     * @return the new initialized page object
     */
    public PageObject createPageObject(PageObjectFactory factory) {
        PageObject pageObject = factory.createInstance()
        // initializing it
        initPageObject(pageObject)
        return pageObject
    }

    /**
     * Checks if the given page object is available or not using {@link PageObject#isAvailable()} method.
     * It respects the mechanism of ignoring exception borrowed from Selenium, so it means there's no need
     * to catch ignored ones when checking page object availability. The provided class is expected to have
     * a no-arg constructor.
     *
     * @param pageClass the page object's class for checking availability for
     * @return true is page object is available, false otherwise
     */
    public <T extends PageObject> boolean isAvailable(Class<T> pageClass) {
        assert PageObject.isAssignableFrom(pageClass), "The given class is not a PageObject"
        isAvailable(pageClass.newInstance())
    }

    /**
     * Checks if the given page object is available or not using {@link PageObject#isAvailable()} method.
     * It respects the mechanism of ignoring exception borrowed from Selenium, so it means there's no need
     * to catch ignored ones when checking page object availability.
     *
     * @param page the page object to check availability of
     * @return true is page object is available, false otherwise
     */
    public boolean isAvailable(PageObject page) {
        assert PageObject.isInstance(page), "The given object is not a PageObject instance"
        page.setWebDriver(webDriver)
        WebDriverWait wait = new ExtendedWebDriverWait(webDriver, 0, 0) // for immediate check
        try {
            return (boolean) wait.until { // casting as the return type is known here
                page.isAvailable()
            }
        } catch (TimeoutException e) {
            // unavailable and exception was ignored
            return false
        } catch (Exception e) {
            // the exception is not ignored so rethrowing it
            throw e
        }
    }

    /**
     * Initializes the given page object instance.
     *
     * @param page the page object to initialize
     * @return the initialized page object
     * @throws NullPointerException if the given page object is null
     */
    public <T extends PageObject> T initPageObject(T page) {
        assert PageObject.isInstance(page), "The given object is not a PageObject instance"

        // injecting WebDriver if needed
        if (!page.webDriver) {
            page.setWebDriver(webDriver)
        }

        // initializing JS proxy object
        new PageObjectJavascriptManager(page).initJsProxy()

        // waiting until the page is available
        wait.untilAvailable(page)

        // search context for the page object
        SearchContext searchContext = null

        // waiting until the page's root context is available
        WebElement rootElement = null
        wait.until({
            rootElement = page.getRootElement()
            if (!rootElement) {
                searchContext = (SearchContext) webDriver
            } else {
                searchContext = (SearchContext) rootElement
            }
        }, "Failed to wait for the root context of the page object '${page.class.name}'")

        // creating new locator factory for the page object's root context
        def locatorFactory = elementLocatorFactoryClass.newInstance(searchContext)

        // ask Selenium's PageFactory to initialize its part
        PageFactory.initElements(new ExtendedDefaultFieldDecorator(locatorFactory), page)

        // calling init method of the page object
        page.init()

        page.setBuilder(this)

        page
    }

    /**
     * Gets wait object for this builder.
     *
     * @return the wait object
     */
    public PageObjectBuilderWaitWrapper getWait() {
        wait
    }

}
