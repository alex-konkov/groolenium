package org.groolenium.framework.page

import org.openqa.selenium.SearchContext
import org.openqa.selenium.support.pagefactory.ElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory

import java.lang.reflect.Field

/**
 * Implementation of {@link ElementLocatorFactory}
 * that respects contract supplied by {@link IgnoreBinding} annotation for page object fields.
 *
 * <p>Other details of {@link ElementLocatorFactory} behavior is the same as
 * {@link org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory}.</p>
 *
 *
 *
 * @see IgnoreBinding
 * @see org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory
 */
class IgnoreBindingAwareElementLocatorFactory implements ElementLocatorFactory {

    private final ElementLocatorFactory factory;

    public IgnoreBindingAwareElementLocatorFactory(SearchContext searchContext) {
        factory = new ExtendedDefaultElementLocatorFactory(searchContext);
    }

    public ElementLocator createLocator(Field field) {
        if (shouldLocate(field)) {
            factory.createLocator(field)
        }
        else {
            null
        }
    }

    boolean shouldLocate(Field field) {
        !field.getAnnotation(IgnoreBinding.class)
    }

}
