package org.groolenium.framework

/**
 * Centralized configuration which can be loaded from the groovy DSL file. The configuration location is read from
 * the system property {@link Configuration#CONFIG_SYSPROP_NAME}, if not provided then
 * {@link Configuration#CONFIG_RESOURCE_NAME} will be taken as a config, if available in the classpath.
 *
 * <p>The base configuration is loaded from the {@link Configuration#DEFAULT_CONFIG_RESOURCE_NAME} resource.
 *
 * <p>Configuration values can be overridden using the system properties. E.g. for configuration like
 * <tt>one{two=2}</tt> setting system property <tt>one.two</tt> will change the value. Note that when
 * applied, changing values get internally converted to the types of the original values.
 *
 * @author Alex K
 */
class Configuration {

    /**
     * Configuration file resource.
     */
    private static final String CONFIG_RESOURCE_NAME = 'config.groovy'

    /**
     * Resource name with the base configuration.
     */
    private static final String DEFAULT_CONFIG_RESOURCE_NAME = 'default_config'

    /**
     * Name of the system property for getting config location from.
     */
    private static final String CONFIG_SYSPROP_NAME = 'webdriver.framework.config'

    /**
     * Global configuration singleton.
     */
    private static Configuration config

    /**
     * Configuration object for retrieving data from.
     */
    private ConfigObject configObject

    /**
     * Flattened version of the configuration.
     */
    private Map configFlat

    /**
     * Constructor which creates the config from the given string.
     *
     * @param configString the config string
     */
    Configuration(String configString) {
        this.configObject = buildMergedConfigObject(configString)
    }

    /**
     * Constructor which creates the config from the given config object.
     *
     * @param configString
     */
    Configuration(ConfigObject configObject) {
        this.configObject = configObject
    }

    /**
     * Returns dynamic configuration object for the current configuration.
     *
     * @return the configuration object
     */
    ConfigObject getConfigObject() {
        configObject
    }

    /**
     * Returns configuration in flat format as a Java map.
     *
     * @return the configuration map
     */
    Map getConfigFlat() {
        configFlat
    }

    /**
     * Returns configuration object for the global configuration.
     *
     * @return the configuration object
     */
    static ConfigObject get() {
        if (!config) {
            config = build()
        }
        config.configObject
    }

    /**
     * Returns flattened version of the configuration represented as a map.
     *
     * @return the configuration map
     */
    static Map getFlat() {
        if (!config) {
            config = build()
        }
        config.configFlat
    }

    /**
     * Helping method for translating either class name or class object to a class instance.
     *
     * @param value the value which is either a fully qualified class name or a class instance
     * @return the class instance
     */
    static Class<?> toClass(Object value) {
        Class factoryClass
        if (value instanceof String || value instanceof GString) {
            factoryClass = Class.forName(value.toString())
        } else if (value instanceof Class) {
            factoryClass = value
        } else {
            throw new IllegalArgumentException("Unsupported class value '${value}' of type '${value.class.name}'")
        }
        factoryClass
    }

    /**
     * Builds a configuration instance.
     *
     * @return the configuration
     */
    private static Configuration build() {
        // attempt to load config from the file specified through the system property
        config = buildFromSyspropLocation()
        if (!config) { // still not loaded?
            // attempt to load config from the resource in classpath
            config = buildFromClasspathResource()
        }
        if (!config) { // still not loaded?
            // loading just framework's default config
            config = new Configuration(getDefaultConfigObject())
        }
        assert config, 'Failed to initialize configuration'
        config.configFlat = config.configObject.flatten()
        config
    }

    /**
     * Builds config object from the given string with applied system properties.
     *
     * @param configString the built config object
     */
    private static ConfigObject buildMergedConfigObject(String configString) {
        ConfigObject defaultConfig = getDefaultConfigObject()
        ConfigObject configObject = new ConfigSlurper().parse(configString)
        defaultConfig.merge(configObject)      // merging the given config into the default one
        applySystemPropertiesTo(defaultConfig) // applying system properties
        defaultConfig
    }

    /**
     * Applies system properties onto the respective entries of the given config object.
     *
     * @param configObject the config object to override items in
     * @return the config object with applied system properties
     */
    private static def applySystemPropertiesTo(configObject) {
        def flattened = configObject.toProperties()
        flattened.each {
            def sysProp = System.getProperty(it.key)
            if (sysProp) {
                Eval.x(configObject, "x.${it.key}='${sysProp}'.asType(x.${it.key}.class)")
            }
        }
    }

    /**
     * Builds default config instance, if available in the classpath.
     *
     * @return config instance or <tt>null</tt> if there's no default config available in classpath
     */
    private static Configuration buildFromClasspathResource() {
        def defaultConfig = Configuration.class.getClassLoader().getResource(CONFIG_RESOURCE_NAME)
        if (defaultConfig) {
            // default config found, so reading from it
            String configStr = defaultConfig.text
            return new Configuration(configStr)
        }
        null // no default config found
    }

    /**
     * Builds config instance from the file at the location provided via system property {@link #CONFIG_SYSPROP_NAME}.
     *
     * @return config instance or <tt>null</tt> if there was no system property passed in
     */
    private static Configuration buildFromSyspropLocation() {
        String configLocation = System.getProperty(CONFIG_SYSPROP_NAME)
        if (configLocation) {
            def configFile = new File(configLocation)
            assert configFile.exists(), "Configuration file '${configLocation}' doesn't exist"
            return new Configuration(configFile.text)
        }
        null // no config found at the specified location
    }

    /**
     * Gets a default config object that is always loaded as a basis config.
     *
     * @return the configuration object of the default config
     */
    private static ConfigObject getDefaultConfigObject() {
        URL url = Configuration.getClassLoader().getResource(DEFAULT_CONFIG_RESOURCE_NAME)
        assert url, "Couldn't find default config resource"
        new ConfigSlurper().parse(url);
    }

}
