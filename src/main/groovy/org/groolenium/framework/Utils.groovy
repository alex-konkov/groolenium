package org.groolenium.framework

import java.util.concurrent.Callable

/**
 * Various utility methods.
 *
 * @author Alex K
 */
class Utils {

    /**
     * Gets the execution time of a given code.
     *
     * @param code the code to execute
     * @return the execution time in ms
     */
    static long getExecutionTimeMs(Callable<?> code) {
        def before = System.currentTimeMillis()
        code.call()
        System.currentTimeMillis() - before
    }

}
