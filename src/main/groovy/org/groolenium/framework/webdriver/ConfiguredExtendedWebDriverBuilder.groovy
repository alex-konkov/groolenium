package org.groolenium.framework.webdriver

import org.openqa.selenium.WebDriver

/**
 * Builds configured extended web driver.
 *
 * @author Alex K
 */
class ConfiguredExtendedWebDriverBuilder {

    WebDriver webDriver

    ConfiguredExtendedWebDriverBuilder(WebDriver webDriver) {
        this.webDriver = webDriver
    }

    ExtendedWebDriver build() {
        def waitBuilder = new ConfiguredExtendedWebDriverWaitBuilder(webDriver)
        new ExtendedWebDriver(webDriver, waitBuilder);
    }

}
