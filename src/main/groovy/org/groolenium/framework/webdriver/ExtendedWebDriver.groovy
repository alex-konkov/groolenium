package org.groolenium.framework.webdriver

import groovy.util.logging.Log
import org.openqa.selenium.*
import org.openqa.selenium.interactions.HasInputDevices
import org.openqa.selenium.interactions.Keyboard
import org.openqa.selenium.interactions.Mouse
import org.openqa.selenium.internal.WrapsDriver
import org.openqa.selenium.remote.Command
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.remote.Response

import java.util.logging.Level

/**
 * Extension for WebDriver with additional capabilities.
 *
 * @author Alex K
 */
@Log
class ExtendedWebDriver extends ExtendedSearchContext implements WebDriver, JavascriptExecutor, TakesScreenshot,
        HasInputDevices, WrapsDriver {

    private WebDriver webDriver

    private ExtendedWebDriverWaitBuilder waitBuilder

    public ExtendedWebDriver(WebDriver webDriver, ExtendedWebDriverWaitBuilder waitBuilder) {
        super(webDriver)
        if (!webDriver) {
            throw new IllegalArgumentException("WebDriver is null")
        }
        // rejecting implementations which are not capable of executing Javascript
        if (!(webDriver instanceof JavascriptExecutor)) {
            throw new IllegalArgumentException("must implement JavascriptExecutor")
        }
        // rejecting implementations which do not provide input devices
        if (!(webDriver instanceof HasInputDevices)) {
            throw new IllegalArgumentException("must implement HasInputDevices")
        }
        this.webDriver = webDriver
        this.waitBuilder = waitBuilder
    }

    /**
     * Convenience method for evaluating expressions omitting explicit 'return' statement in JS.
     *
     * @param script the expression script to execute
     * @param args the arguments for the script
     * @return the evaluation result
     */
    Object executeScriptReturn(String script, Object... args) {
        executeScript("return $script;", args)
    }

    // convenience methods for waiting {

    /**
     * Gets an extended wait object configured with the given timeout and polling period.
     *
     * @param timeoutSec the timeout in seconds to use
     * @param pollingPeriodMs the polling period in milliseconds to use
     * @return the wait object
     */
    ExtendedWebDriverWait getWait(long timeoutSec, long pollingPeriodMs) {
        new ExtendedWebDriverWait(this, timeoutSec, pollingPeriodMs)
    }

    /**
     * Gets an extended wait object for relatively quick operations.
     *
     * @return the short wait object
     */
    ExtendedWebDriverWait getShortWait() {
        waitBuilder.buildShortWait()
    }

    /**
     * Gets an extended wait object for relatively slow operations.
     *
     * @return the long wait object
     */
    ExtendedWebDriverWait getLongWait() {
        waitBuilder.buildLongWait()
    }

    // } convenience methods for waiting

    // script execution redefined {

    @Override
    Object executeScript(String script, Object... args) {
        executeScriptAndLogOnError({
            ((JavascriptExecutor) webDriver).executeScript(script, args)
        }, script, args)
    }

    @Override
    Object executeAsyncScript(String script, Object... args) {
        executeScriptAndLogOnError({
            ((JavascriptExecutor) webDriver).executeAsyncScript(script, args)
        }, script, args)
    }

    // } script execution redefined

    // convenience redefinition of screenshot taking {

    @Override
    def <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        // if the wrapped webDriver has the facility then just delegating the invocation
        if (webDriver instanceof TakesScreenshot) {
            return ((TakesScreenshot) webDriver).getScreenshotAs(target)
        }
        // screenshot workarounds for remote driver
        if (webDriver instanceof RemoteWebDriver) {
            if (!OutputType.BASE64.equals(target)) { // reject all requested output types but base64
                throw new IllegalArgumentException("Unsupported output type, the only supported one is base64")
            }
            RemoteWebDriver rwd = (RemoteWebDriver) webDriver
            Response rs = rwd.commandExecutor.execute(new Command(rwd.getSessionId(), "screenshot"))
            if (rs.value.class == String) {
                return (X)rs.value
            } else {
                throw new IllegalArgumentException("Unexpected command response type: '${rs.value.class}', base64 expected")
            }
        }
        throw new WebDriverException("Unable to take a screenshot in the current configuration")
    }

    // } convenience redefinition of screenshot taking

    @Override
    void get(String url) {
        webDriver.get(url)
    }

    @Override
    String getCurrentUrl() {
        webDriver.getCurrentUrl()
    }

    @Override
    String getTitle() {
        webDriver.getTitle()
    }

    @Override
    List<ExtendedWebElement> findElements(By by) {
        ExtendedWebElement.wrapIfNeeded(webDriver.findElements(by))
    }

    @Override
    ExtendedWebElement findElement(By by) {
        ExtendedWebElement.wrapIfNeeded(webDriver.findElement(by))
    }

    @Override
    String getPageSource() {
        webDriver.getPageSource()
    }

    @Override
    void close() {
        webDriver.close()
    }

    @Override
    void quit() {
        webDriver.quit()
    }

    @Override
    Set<String> getWindowHandles() {
        webDriver.getWindowHandles()
    }

    @Override
    String getWindowHandle() {
        webDriver.getWindowHandle()
    }

    @Override
    WebDriver.TargetLocator switchTo() {
        webDriver.switchTo()
    }

    @Override
    WebDriver.Navigation navigate() {
        webDriver.navigate()
    }

    @Override
    WebDriver.Options manage() {
        webDriver.manage()
    }

    @Override
    Keyboard getKeyboard() {
        ((HasInputDevices) webDriver).getKeyboard()
    }

    @Override
    Mouse getMouse() {
        ((HasInputDevices) webDriver).getMouse()
    }

    /**
     * {@inheritDoc}
     */
    @Override
    WebDriver getWrappedDriver() {
        webDriver
    }

    @Override
    def <T> T newJSObject(Class<T> type, CharSequence source) {
        jsObjectWrapperFactory.newJSObject(type, source)
    }

    /**
     * Returns extended actions object for this WebDriver instance.
     *
     * @return the extended actions object
     */
    ExtendedActions getActions() {
        new ExtendedActions(this)
    }

    /**
     * Runs the given code that has to execute some remote Javascript and logs an exception, if thrown.
     *
     * @param code the code to execute
     * @param script the script to execute by the given code
     * @param args the script arguments to use
     * @return the script invocation result
     *
     * TODO: make this capability configurable !!!
     *
     */
    private Object executeScriptAndLogOnError(Closure code, String script, Object... args) {
        try {
            return code.call()
        } catch (e) {
            log.log(Level.WARNING, "Exception occurred when executing script '${script}' with args '${args}'", e)
            throw e
        }
    }

}
