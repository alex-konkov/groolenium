package org.groolenium.framework.webdriver

import org.groolenium.framework.Configuration
import org.openqa.selenium.WebDriver

/**
 * Builders for creating configured WebDriver wait object. Configuration example:
 *
 * <p>
 * {@code
 * extendedWebDriver.waitConfiguration.shortWait.timeoutSec = 5
 * extendedWebDriver.waitConfiguration.longWait.timeoutSec = 30
 * extendedWebDriver.waitConfiguration.pollingPeriodMs = 1000
 * }
 *
 * @author Alex K
 */
class ConfiguredExtendedWebDriverWaitBuilder implements ExtendedWebDriverWaitBuilder {

    private WebDriver driver

    ConfiguredExtendedWebDriverWaitBuilder(WebDriver driver) {
        this.driver = driver
    }

    ExtendedWebDriverWait buildShortWait() {
        new ExtendedWebDriverWait(driver,
                getConfig()['shortWait']['timeoutSec'] as long,
                getConfig()['pollingPeriodMs'] as long)
    }

    ExtendedWebDriverWait buildLongWait() {
        new ExtendedWebDriverWait(driver,
                getConfig()['longWait']['timeoutSec'] as long,
                getConfig()['pollingPeriodMs'] as long)
    }

    private static def getConfig() {
        Configuration.get()['webDriverFramework']['extendedWebDriver']['waitConfiguration']
    }

}
