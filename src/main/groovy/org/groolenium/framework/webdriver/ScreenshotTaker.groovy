package org.groolenium.framework.webdriver

import org.apache.commons.codec.binary.Base64
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriverException

/**
 * Stands for taking screenshots through Selenium API and storing them into the file system.
 *
 * @author Alex K
 */
class ScreenshotTaker {

    private TakesScreenshot driver

    ScreenshotTaker(TakesScreenshot driver) {
        this.driver = driver
    }

    /**
     * Takes a screenshot with the given key storing it into the given output directory.
     *
     * @param key the screenshot parameter representing a context where it's taken
     * @param outputDir the output directory for storing a screenshot file
     * @return the newly created screenshot file, or 'null' if the screenshot is not taken for whatever reason
     */
    File takeScreenshot(String key, File outputDir) {
        String base64Screenshot
        try {
            base64Screenshot = driver.getScreenshotAs(OutputType.BASE64)
        } catch (WebDriverException e) {
            println "Couldn't take a screenshot, see exception below"
            e.printStackTrace()
            return null // silently returning 'null'
        }
        outputDir.mkdirs() // create dir(s) if it doesn't exist
        String imageFileName = "${key}_${System.currentTimeMillis()}.png"
        byte[] decodedScreenshot = Base64.decodeBase64(base64Screenshot.getBytes())
        File imageFile = new File(outputDir, imageFileName)
        imageFile.withOutputStream { fos ->
            fos.write(decodedScreenshot)
        }
        println "stored OS screenshot for '${key}' at '${imageFile.getAbsolutePath()}'"
        imageFile
    }

}
