package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver

/**
 * Factory for local Safari WebDriver implementation.
 *
 * @author Alex K
 */
public class LocalSafariWebDriverFactory extends CapabilitiesAwareWebDriverFactory {

    LocalSafariWebDriverFactory(Capabilities desiredCapabilities) {
        super(desiredCapabilities)
    }

    @Override
    WebDriver createWebDriver() {
        def driverClass = Class.forName('org.openqa.selenium.safari.SafariDriver')
        driverClass.newInstance(desiredCapabilities)
    }

}