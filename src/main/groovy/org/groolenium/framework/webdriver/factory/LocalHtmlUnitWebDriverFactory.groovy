package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver

/**
 * Factory for HtmlUnit WebDriver implementation.
 *
 * @author Alex K
 */
class LocalHtmlUnitWebDriverFactory extends CapabilitiesAwareWebDriverFactory {

    LocalHtmlUnitWebDriverFactory(Capabilities desiredCapabilities) {
        super(desiredCapabilities)
    }

    @Override
    WebDriver createWebDriver() {
        def driverClass = Class.forName('org.openqa.selenium.htmlunit.HtmlUnitDriver')
        driverClass.newInstance(desiredCapabilities)
    }

}
