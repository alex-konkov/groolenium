package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver

/**
 * For Opera WebDriver running locally.
 *
 * @author Alex K
 */
class LocalOperaDriverFactory extends CapabilitiesAwareWebDriverFactory {

    LocalOperaDriverFactory(Capabilities desiredCapabilities) {
        super(desiredCapabilities)
    }

    @Override
    WebDriver createWebDriver() {
        def driverClass = Class.forName('com.opera.core.systems.OperaDriver')
        driverClass.newInstance(desiredCapabilities)
    }

}
