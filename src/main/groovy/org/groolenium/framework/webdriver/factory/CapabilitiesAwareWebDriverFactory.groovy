package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities

/**
 * Factory that is aware of the desired capabilities for the WebDriver.
 *
 * @author Alex K
 */
abstract class CapabilitiesAwareWebDriverFactory implements WebDriverFactory {

    protected Capabilities desiredCapabilities

    CapabilitiesAwareWebDriverFactory(Capabilities desiredCapabilities) {
        this.desiredCapabilities = desiredCapabilities
    }

}
