package org.groolenium.framework.webdriver.factory

import org.groolenium.framework.Configuration
import org.openqa.selenium.Capabilities
import org.openqa.selenium.Platform
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.DesiredCapabilities

/**
 * Gets configured WebDriver factory.
 *
 * @author Alex K
 */
class ConfiguredWebDriverFactory extends CapabilitiesAwareWebDriverFactory {

    ConfiguredWebDriverFactory() {
        super(new DesiredCapabilities())
    }

    ConfiguredWebDriverFactory(Capabilities desiredCapabilities) {
        super(desiredCapabilities)
    }

    @Override
    WebDriver createWebDriver() {
        Class factoryClass = Configuration.toClass(getConfig()['factoryClass'])
        assert WebDriverFactory.isAssignableFrom(factoryClass), "factoryClass is not a WebDriverFactory"
        Map commonCapabilitiesMap = getConfig()['capabilities'] // Map is expected as a value!
        if (commonCapabilitiesMap == null) {
            commonCapabilitiesMap = [:]
        }
        DesiredCapabilities commonCapabilities = new DesiredCapabilities(commonCapabilitiesMap)
        if (factoryClass == RemoteWebDriverFactory) {
            def remoteConfig = getConfig()['remoteConfig']
            String url = remoteConfig['url']
            String desiredPlatform = remoteConfig['desiredPlatform']
            String desiredBrowser = remoteConfig['desiredBrowser']
            Platform platform = Platform.extractFromSysProperty(desiredPlatform)
            def remoteCapabilities = new DesiredCapabilities(desiredBrowser, '', platform)
            commonCapabilities.merge(remoteCapabilities) // remote ones take precedence
            return new RemoteWebDriverFactory(new URL(url), commonCapabilities).createWebDriver()
        }
        WebDriverFactory newFactory
        // TODO: rework this!!!
        if (CapabilitiesAwareWebDriverFactory.isAssignableFrom(factoryClass)) {
            newFactory = (WebDriverFactory) factoryClass.newInstance(commonCapabilities)
        } else {
            newFactory = (WebDriverFactory) factoryClass.newInstance()
        }
        return newFactory.createWebDriver()
    }

    private ConfigObject getConfig() {
        (ConfigObject) Configuration.get()['webDriverFramework']['webDriverFactory']
    }


}
