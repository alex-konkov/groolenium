package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver

/**
 * Factory for remote WebDriver implementation.
 *
 * @author Alex K
 */
class RemoteWebDriverFactory extends CapabilitiesAwareWebDriverFactory {

    private final URL url;
    
    RemoteWebDriverFactory(final URL url, final Capabilities desiredCapabilities) {
        super(desiredCapabilities)
        this.url = url;
    }

    @Override
    WebDriver createWebDriver() {
        def driverClass = Class.forName('org.openqa.selenium.remote.RemoteWebDriver')
        driverClass.newInstance(url, desiredCapabilities)
    }

}
