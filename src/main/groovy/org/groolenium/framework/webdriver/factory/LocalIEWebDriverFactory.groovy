package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver

/**
 * For Internet Explorer running locally.
 *
 * @author Alex K
 */
class LocalIEWebDriverFactory extends CapabilitiesAwareWebDriverFactory {

    LocalIEWebDriverFactory(Capabilities desiredCapabilities) {
        super(desiredCapabilities)
    }

    @Override
    WebDriver createWebDriver() {
        def driverClass = Class.forName('org.openqa.selenium.ie.InternetExplorerDriver')
        driverClass.newInstance(desiredCapabilities)
    }

}
