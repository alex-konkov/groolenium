package org.groolenium.framework.webdriver.factory

import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver

/**
 * For development only, the factory has to produce Chrome WebDriver. Note that Chromedriver executable
 * is expected to be in the system 'PATH'.
 *
 * @author Alex K
 */
class LocalChromeWebDriverFactory extends CapabilitiesAwareWebDriverFactory {

    LocalChromeWebDriverFactory(Capabilities desiredCapabilities) {
        super(desiredCapabilities)
    }

    @Override
    WebDriver createWebDriver() {
        def driverClass = Class.forName('org.openqa.selenium.chrome.ChromeDriver')
        driverClass.newInstance(desiredCapabilities)
    }

}
