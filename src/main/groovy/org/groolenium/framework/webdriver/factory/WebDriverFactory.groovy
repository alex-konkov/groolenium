package org.groolenium.framework.webdriver.factory;

import org.openqa.selenium.WebDriver;

/**
 * Factory for producing WebDriver instance.
 *
 * @author Alex K
 */
interface WebDriverFactory {

    /**
     * Creates new WebDriver instance.
     *
     * @return the created WebDriver instance
     */
    WebDriver createWebDriver();

}
