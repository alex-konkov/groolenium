package org.groolenium.framework.webdriver

import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait

import java.util.concurrent.Callable

/**
 * Extension with additional groovy capabilities.
 *
 * @author Alex K
 */
class ExtendedWebDriverWait extends WebDriverWait {

    WebDriver driver

    private long timeoutSec

    private long sleepBetweenPollsMs

    public ExtendedWebDriverWait(WebDriver driver, long timeoutSec, long sleepBetweenPollsMs) {
        super(driver, timeoutSec, sleepBetweenPollsMs)
        this.driver = driver
        this.timeoutSec = timeoutSec
        this.sleepBetweenPollsMs = sleepBetweenPollsMs
    }

    /**
     * Convenience method, takes callable condition.
     *
     * @param code the callable condition
     * @return the condition invocation result
     */
    public def until(Callable<?> code) {
        until(new ExpectedCondition<Object>() {
            @Override
            Object apply(WebDriver o) {
                code.call()
            }
        })
    }

    /**
     * Waits until the browser goes to another page.
     *
     * @param code the callable code which is expected to direct browser to another page
     */
    public void untilNavigatedAway(Callable<?> code) {
        if (!(driver instanceof JavascriptExecutor)) {
            throw new IllegalStateException("WebDriver is not a JavascriptExecutor")
        }
        JavascriptExecutor je = (JavascriptExecutor) driver
        final String uidVarJS = "window['" + UUID.randomUUID() + "']";
        // defining the variable
        je.executeScript("${uidVarJS}=true;")
        try {
            code.call();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected exception while executing the given code", e);
        }
        // waiting until the variable becomes undefined
        until({
            je.executeScript("return typeof(${uidVarJS})==='undefined'")
        }, "The browser was not navigated to another page")
    }

    private String appendTimeoutSuff(String errorMessage) {
        errorMessage + " - timeout: ${getTimeoutSec()} sec"
    }

    /**
     * This convenience method version allows to specify exception message in case of timeout.
     *
     * @param code the callable code
     * @param errorMessage the error message which will be put into thrown exception
     * @return the closure invocation result
     */
    public def until(Callable<?> code, String errorMessage) {
        try {
            return until(code)
        } catch (TimeoutException e) {
            throw new TimeoutException(appendTimeoutSuff(errorMessage), e)
        }
    }

    public long getTimeoutSec() {
        timeoutSec
    }

    public long getSleepBetweenPollsMs() {
        sleepBetweenPollsMs
    }

    public long getTimeoutMs() {
        timeoutSec * 1000
    }

}
