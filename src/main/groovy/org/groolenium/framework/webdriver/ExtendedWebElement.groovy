package org.groolenium.framework.webdriver

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.internal.Coordinates
import org.openqa.selenium.internal.Locatable
import org.openqa.selenium.internal.WrapsElement

/**
 * Wrapper for web element with additional capabilities.
 *
 * @author Alex K
 */
class ExtendedWebElement extends ExtendedSearchContext implements WebElement, WrapsElement, Locatable {

    @Delegate
    WebElement element

    ExtendedWebElement(WebElement element) {
        super(element)
        if (!(element instanceof Locatable)) {
            throw new IllegalArgumentException('The wrapped element is not locatable')
        }
        this.element = element
    }

    @Override
    ExtendedWebElement findElement(By by) {
        wrapIfNeeded(element.findElement(by))
    }

    @Override
    List<ExtendedWebElement> findElements(By by) {
        wrapIfNeeded(element.findElements(by))
    }

    @Override
    WebElement getWrappedElement() {
        element
    }

    static ExtendedWebElement wrapIfNeeded(WebElement el) {
        el instanceof ExtendedSearchContext ? el : new ExtendedWebElement(el)
    }

    static List<ExtendedWebElement> wrapIfNeeded(List<WebElement> els) {
        els.collect { wrapIfNeeded(it) }
    }

    @Override
    public boolean equals(Object obj) {
        element.equals(obj)
    }

    @Override
    public int hashCode() {
        element.hashCode();
    }

    @Override
    Coordinates getCoordinates() {
        ((Locatable)element).getCoordinates()
    }

}