package org.groolenium.framework.webdriver.exception

import org.openqa.selenium.WebDriverException

/**
 * Thrown to indicate that an element is not unique.
 *
 * @author Alex K
 */
class ElementNotUniqueException extends WebDriverException {

    public ElementNotUniqueException() {
    }

    public ElementNotUniqueException(String message) {
        super(message);
    }

    public ElementNotUniqueException(String message, Throwable cause) {
        super(message, cause);
    }

    public ElementNotUniqueException(Throwable cause) {
        super(cause);
    }

}

