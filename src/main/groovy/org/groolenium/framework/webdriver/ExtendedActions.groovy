package org.groolenium.framework.webdriver

import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions

/**
 * Customized WebDriver actions.
 *
 * @author Alex K
 */
class ExtendedActions extends Actions {

    ExtendedWebDriver webDriver

    ExtendedActions(ExtendedWebDriver webDriver) {
        super(webDriver)
        this.webDriver = webDriver
    }

    ExtendedActions doubleClickUsingDomEvents(WebElement onElement) {
        2.times {
            addMouseAction 'click', onElement
        }
        addMouseAction 'dblclick', onElement
        this
    }

    ExtendedActions clickUsingDomEvents(WebElement onElement) {
        ['mousedown', 'mouseup', 'click'].each { String mouseEvent ->
            addMouseAction mouseEvent, onElement
        }
        this
    }

    ExtendedActions mouseDownUsingDomEvents(WebElement onElement) {
        addMouseAction 'mousedown', onElement
        this
    }

    ExtendedActions mouseUpUsingDomEvents(WebElement onElement) {
        addMouseAction 'mouseup', onElement
        this
    }

    private Action addMouseAction(String type, WebElement onElement) {
        action.addAction({
            webDriver.executeScript(getJsForClickUsingDomEvents(type), onElement)
        } as Action)
    }

    private def getJsForClickUsingDomEvents(clickType) {
        """
            var evt = document.createEvent('MouseEvents');
            evt.initMouseEvent('$clickType',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            arguments[0].dispatchEvent(evt);
        """.trim()
    }

}
