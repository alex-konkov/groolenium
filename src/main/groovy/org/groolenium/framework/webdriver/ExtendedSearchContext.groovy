package org.groolenium.framework.webdriver

import org.groolenium.framework.webdriver.exception.ElementNotUniqueException
import org.openqa.selenium.By
import org.openqa.selenium.SearchContext
import org.openqa.selenium.WebElement

/**
 * Wrapper of WebElement object which brings additional functionality.
 *
 * @author Alex K
 */
abstract class ExtendedSearchContext implements SearchContext {

    @Delegate
    private SearchContext context

    ExtendedSearchContext(SearchContext context) {
        this.context = context
    }

    /**
     * Finds the single {@link WebElement} using the given method.
     *
     * @param by the locating mechanism
     * @return the matching element on the current page
     * @throws org.openqa.selenium.NoSuchElementException if no matching elements are found
     * @throws ElementNotUniqueException if there are several elements found
     * @see #findElement(org.openqa.selenium.By)
     */
    ExtendedWebElement findSingleElement(By by) {
        List<WebElement> found = findElements(by)
        if (found.isEmpty()) {
            throw new org.openqa.selenium.NoSuchElementException("Unable to locate an element '$by'");
        }
        if (found.size() > 1) {
            throw new ElementNotUniqueException("Element '$by' is not unique")
        }
        found[0]
    }

}
