package org.groolenium.framework.webdriver.by

import org.openqa.selenium.*
import org.openqa.selenium.internal.WrapsDriver

/**
 * Finding method using JQuery selector.
 *
 * <p>Usage example:
 * <pre>
 *     webDriver.findElements(ByJQuery.selector(".some-class"))
 * </pre>
 *
 * @author Alex K
 */
class ByJQuery extends By {

    private final String selector;

    private final String quotedSelector

    /**
     * Gets new @{@link By} instance for the given JQuery selector.
     *
     * @param selector the JQuery selector to use
     * @return the @{@link By} instance
     */
    static ByJQuery selector(String selector) {
        new ByJQuery(selector)
    }

    /**
     * Private constructor. The creation has to be done using factory method {@link #selector(java.lang.String)}.
     *
     * @param selector the JQuery selector to use
     * @see #selector(java.lang.String)
     */
    private ByJQuery(String selector) {
        this.quotedSelector = quote(selector)
        this.selector = selector;
    }

    @Override
    List findElements(SearchContext context) {
        List result
        JavascriptExecutor jsEx = resolveJavascriptExecutorBy(context)
        def jqueryPresenceAwareExecutor = new JQueryAvailabilityAwareJSExecutor(jsEx)
        if (isWebElementBased(context)) {
            result = (List) jqueryPresenceAwareExecutor.executeScript(
                    "return jQuery(arguments[0]).find(${quotedSelector}).get();",
                    (WebElement) context
            )
        } else if (isWebDriverBased(context)) {
            result = (List) jqueryPresenceAwareExecutor.executeScript(
                    "return jQuery(${quotedSelector}).get();"
            )
        } else {
            throw new IllegalStateException("Unrecognized search context")
        }
        result
    }

    @Override
    WebElement findElement(SearchContext context) {
        WebElement element
        JavascriptExecutor jsEx = resolveJavascriptExecutorBy(context)
        def jqueryPresenceAwareExecutor = new JQueryAvailabilityAwareJSExecutor(jsEx)
        if (isWebElementBased(context)) {
            element = (WebElement) jqueryPresenceAwareExecutor.executeScript(
                    "return jQuery(arguments[0]).find(${quotedSelector}).get(0);",
                    (WebElement) context
            )
        } else if (isWebDriverBased(context)) {
            element = (WebElement) jqueryPresenceAwareExecutor.executeScript(
                    "return jQuery(${quotedSelector}).get(0);"
            )
        } else {
            throw new IllegalStateException("Unrecognized search context")
        }
        if (element == null) {
            throw new NoSuchElementException("No element found for JQuery selector '${selector}'");
        }
        return element;
    }

    @Override
    String toString() {
        "ByJQuery.selector: $selector"
    }

    /**
     * Returns the Javascript executor for the given search context.
     *
     * @param context the context to resolve Javascript executor for
     * @return the resolved {@link JavascriptExecutor}
     */
    private static JavascriptExecutor resolveJavascriptExecutorBy(SearchContext context) {
        JavascriptExecutor result
        if (isWebElementBased(context) && context instanceof WrapsDriver) {
            WebDriver wrappedDriver = ((WrapsDriver) context).getWrappedDriver()
            result = (JavascriptExecutor) wrappedDriver
        } else if (isWebDriverBased(context)) {
            result = (JavascriptExecutor) context
        } else {
            throw new IllegalArgumentException("Unrecognized search context")
        }
        result
    }

    /**
     * Quotes the given JQuery selector using either single or double quotes depending on which quotes
     * are used within the selector.
     *
     * @param selector the JQuery selector to quote
     * @return the selector which is quoted properly
     */
    private static String quote(String selector) {
        if (!selector.contains('"')) {
            return '"' + selector + '"' // selector contains single quotes => using double ones
        } else if (!selector.contains("'")) {
            return "'" + selector + "'" // selector contains double quotes => using single ones
        } else {
            throw new IllegalArgumentException('Selector must have either single or double quotes only')
        }
    }

    private static boolean isWebDriverBased(SearchContext context) {
        return context instanceof WebDriver
    }

    private static boolean isWebElementBased(SearchContext context) {
        return context instanceof WebElement
    }

    /**
     * Wrapper around {@link JavascriptExecutor} which is able to check if JQuery is loaded.
     */
    private static class JQueryAvailabilityAwareJSExecutor implements JavascriptExecutor {

        @Delegate
        private JavascriptExecutor executor

        JQueryAvailabilityAwareJSExecutor(JavascriptExecutor executor) {
            this.executor = executor
        }

        Object executeScript(String script, Object... args) {
            def result
            try {
                result = executor.executeScript(script, args)
            } catch (Exception e) {
                if (executor.executeScript('return typeof(jQuery)==="undefined";')) {
                    throw new IllegalStateException("JQuery not found, please make sure the page has loaded it", e)
                }
                throw e
            }
            result
        }

    }

}
