package org.groolenium.framework.webdriver

/**
 * Builds extended wait object.
 *
 * @author Alex K
 */
public interface ExtendedWebDriverWaitBuilder {

    ExtendedWebDriverWait buildShortWait();

    ExtendedWebDriverWait buildLongWait();

}