package org.groolenium.framework.webdriver

/**
 * An entity that is aware of the WebDriver instance.
 *
 * @author Alex K
 */
interface ExtendedWebDriverAware {

    ExtendedWebDriver getWebDriver();

}